<?php namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use App\Http\Controllers\DAL\DALController;
use App\Http\Controllers\Business\ProfileController;
use App\Http\Requests;
use Everyman\Neo4j\Cypher\Query;
use Illuminate\Http\Request;


class UserRequestController extends Controller
{

    public function getUsersRequests()
    {
        /*
        This function return the user request for approval.
        */
            $jsonArray = array();
            if(\Session::get('isAdmin') and \Session::get('id')){
                $client = DALController::getConnection();
                $queryString = "MATCH (n:User {adminApproval:false}) WHERE HAS(n.fullName) RETURN (n)";
                $transaction = $client->beginTransaction();
                $query = new Query($client, $queryString);
                $result = $transaction->addStatements($query);
                $transaction -> commit();
            
               
                for ($i = 0; $i < $result->count(); $i++) {
                        $jsonArray[$i] = array('id' => $result[$i][0] -> getId(), 'firstName' => $result[$i][0] ->getProperty('firstName'),
                            'lastName' => $result[$i][0] ->getProperty('lastName'));
                }
            }
            else
            {
                $jsonArray[]=array("errorMessage"=>"You dont have admin rights to access this file.");
            }
            return $jsonArray;
    }

    public function getUsersView()
    {
        /*
        This function return the user request for approval.
        */
            $jsonArray = array();
            if(\Session::get('isAdmin') and \Session::get('id')){
                $client = DALController::getConnection();
                $queryString = "MATCH (n:User {adminApproval:true}) RETURN (n)";
                $transaction = $client->beginTransaction();
                $query = new Query($client, $queryString);
                $result = $transaction->addStatements($query);
                $transaction -> commit();
            
                $jsonArray = array();
                for ($i = 0; $i < $result->count(); $i++) {
                        $jsonArray[$i] = array('id' => $result[$i][0] -> getId(), 'firstName' => $result[$i][0] ->getProperty('firstName'),
                            'lastName' => $result[$i][0] ->getProperty('lastName'), 'isAdmin' => $result[$i][0] -> getProperty('isAdmin'),
                            'isDead' => $result[$i][0] -> getProperty('isDead'));
                }
            }
            else
            {
                $jsonArray[]=array("errorMessage"=>"You dont have admin rights to access this file.");
            }
            return $jsonArray;
    }

    public function getEventsRequests()
    {
        /*
        This function return the event request for approval.
        */
        $jsonArray = array();
        if(\Session::get('isAdmin') and \Session::get('id')){
            $client = DALController::getConnection();
            $queryString = "MATCH (n:User)-[r:Owner*]->(m:Event) where m.adminApproval=false RETURN m, n";
            $transaction = $client->beginTransaction();
            $query = new Query($client, $queryString);
            $result = $transaction->addStatements($query);
            $transaction -> commit();
        
            $jsonArray = array();
            for ($i = 0; $i < $result->count(); $i++) {
                    $jsonArray[$i] = array('id' => $result[$i]['m'] -> getId(), 
                        'eventDate' => $result[$i]['m'] ->getProperty('date'), 
                        'eventName' => $result[$i]['m'] ->getProperty('name'), 
                        'eventDesc' => $result[$i]['m'] ->getProperty('description'),
                        'createdBy' => $result[$i]['n'] ->getProperty('fullName'),
                        'createdById' => $result[$i]['n'] ->getId());
            }
        }
        else
        {
            $jsonArray[]=array("errorMessage"=>"You dont have admin rights to access this file.");
        }
        return $jsonArray;
    }


    public function acceptRequest($id) 
    {
        /*
        Accept Request
        */

        $client = DALController::getConnection();
        $queryString = "start n=node(".$id.") set n.adminApproval=true";
        $transaction = $client->beginTransaction();
        $query = new Query($client, $queryString);
        $result = $transaction->addStatements($query);
        $transaction->commit();
        return \Redirect::to('/admin/views/view-users-request');

    }

    public function acceptRequestEvent($id,$createdById) 
    {
        /*
        Accept Request
        */

        $client = DALController::getConnection();
        $queryString = "start n=node(".$id.") set n.adminApproval=true";
        $transaction = $client->beginTransaction();
        $query = new Query($client, $queryString);
        $result = $transaction->addStatements($query);
        $transaction->commit();

        return \Redirect::to('/admin/views/view-events-request');

    }

    public function addAdminAccess($id) 
    {
        /*
        Accept Request
        */

        $client = DALController::getConnection();
        $queryString = "start n=node(".$id.") set n.isAdmin=true";
        $transaction = $client->beginTransaction();
        $query = new Query($client, $queryString);
        $result = $transaction->addStatements($query);
        $transaction->commit();
        return \Redirect::to('/admin/views/view-users');

    }

    public function removeAdminAccess($id) 
    {
        /*
        Accept Request
        */

        $client = DALController::getConnection();
        $queryString = "start n=node(".$id.") set n.isAdmin=false";
        $transaction = $client->beginTransaction();
        $query = new Query($client, $queryString);
        $result = $transaction->addStatements($query);
        $transaction->commit();
        return \Redirect::to('/admin/views/view-users');

    }

    public function deleteRequest($id,$nodeType) 
    {
        /*
        */
        //var reject = true;
        $client = DALController::getConnection();
        $queryString = "MATCH (user:$nodeType)<-[r]->() WHERE ID(user)=$id DELETE user,r";
        $transaction = $client->beginTransaction();
        $query = new Query($client, $queryString);
        $result = $transaction->addStatements($query);
        $transaction->commit();
        return \Redirect::to('/admin/views/view-users');
    }
}