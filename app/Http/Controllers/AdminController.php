<?php namespace App\Http\Controllers;

use App\Http\Requests;

class AdminController extends Controller
{

    public function addeventsadmin()
    {
        if(\Session::has('mobileNumber') && \Session::get('isAdmin'))
        {
            return view('family-tree-backend/forms/add-events-admin');
        }
        else
        {
            return view('family-tree-backend/login-admin');
        }
    }

    public function addusersadmin()
    {
        if(\Session::has('mobileNumber') && \Session::get('isAdmin'))
        {
            return view('family-tree-backend/forms/add-users-admin');
        }
        else
        {
            return view('family-tree-backend/login-admin');
        }
    }



    public function eventsadmin()
    {
        if(\Session::has('mobileNumber') && \Session::get('isAdmin'))
        {
            return view('family-tree-backend/events-admin');    
        }
        else
        {
            return view('family-tree-backend/login-admin');
        }
        
    }

    public function homeadmin()
    {
        if(\Session::has('mobileNumber') && \Session::get('isAdmin'))
        {
            return view('family-tree-backend/home-admin');
        }
        else
        {
            return view('family-tree-backend/login-admin');
        }
        
    }

    public function loginAdmin()
    {
        return view('family-tree-backend/login-admin');
    }

    public function masteradmin()
    {
        if(\Session::has('mobileNumber') && \Session::get('isAdmin')){
            return view('family-tree-backend/master-admin');    
        }
        else
        {
            return view('family-tree-backend/login-admin');
        }
        
    }

    public function albumsadmin()
    {
        if(\Session::has('mobileNumber') && \Session::get('isAdmin'))
        {
            return view('family-tree-backend/albums-admin');
        }
        else
        {
            return view('family-tree-backend/login-admin');
        }
    }

    public function userstreeadmin()
    {
        if(\Session::has('mobileNumber') && \Session::get('isAdmin'))
        {
            return view('family-tree-backend/users-tree-admin');
        }
        else
        {
            return view('family-tree-backend/login-admin');
        }
    }

    public function vieweventsadmin()
    {
        if(\Session::has('mobileNumber') && \Session::get('isAdmin'))
        {
            \Blade::setEscapedContentTags('[[', ']]');
            \Blade::setContentTags('[[[', ']]]');
            return view('family-tree-backend/views/view-events-admin');
        }
        else
        {
            return view('family-tree-backend/login-admin');
        }

    }

    public function vieweventsrequestadmin()
    {
        if(\Session::has('mobileNumber') && \Session::get('isAdmin'))
        {
            return view('family-tree-backend/views/view-events-request-admin');
        }
        else
        {
            return view('family-tree-backend/login-admin');
        }
    }

    public function viewusersrequestadmin()
    {
        if(\Session::has('mobileNumber') && \Session::get('isAdmin')){
            return view('family-tree-backend/views/view-users-request-admin');
        }
        else{
            return view('family-tree-backend/login-admin');
            // return 'Else';
            // \Redirect::to('\login');
        }
    }

    public function viewusersadmin()
    {
        if(\Session::has('mobileNumber') && \Session::get('isAdmin'))
        {
            return view('family-tree-backend/views/view-users-admin');
        }
        else
        {
            //function loginAdmin()         
            return view('family-tree-backend/login-admin');
        }
    }
}
