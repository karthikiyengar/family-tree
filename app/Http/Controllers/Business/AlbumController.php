<?php namespace App\Http\Controllers\Business;

use App\Http\Requests;
use App\Http\Controllers\Controller;
use App\Http\Controllers\DAL\DALController;
use Illuminate\Http\Request;
use Everyman\Neo4j\Cypher\Query;


class AlbumController extends Controller {

	public static function compress_image($source_url, $destination_url, $quality) {
		$info = \getimagesize($source_url);

		if ($info['mime'] == 'image/jpeg') $image = \imagecreatefromjpeg($source_url);
		elseif ($info['mime'] == 'image/gif') $image = \imagecreatefromgif($source_url);
		elseif ($info['mime'] == 'image/png') $image = \imagecreatefrompng($source_url);

		//save file
		\imagejpeg($image, $destination_url, $quality);

		//return destination file
		return $destination_url;
	}


	public function photoUpload(Request $request) {		
		#moving uploaded file to desination folder
		$random = rand(10000000, 99999999);
		$image= $request->file('file');
		$filename=\Session::get('id')."_".\Session::get('fullName')."$random.".$image->getClientOriginalExtension();
		$destinationPath = "userPhotos/".\Session::get('id');
		$image->move($destinationPath, $filename);

        $destination_url=$destinationPath."/".$filename;
        AlbumController::compress_image($destination_url,$destination_url,75);
        return json_encode(array('status' => 'ok'));
    }

    public function loadPhotos(Request $request) {
		$id = $request -> input ('id');
		if(empty($id)) {
			$id = \Session::get('id');
		}
		$directory = "userPhotos/".$id."/";
		$images = glob($directory . "*.*");
		$photos=array();
		foreach($images as $image)
		{
			$photo=array();
			 $photo['path']=$image;
			 $photos[]=$photo;
		}
		return json_encode($photos);
	}

	public function deleteImage(Request $request) {
		$path= $request->input('path');
		unlink($path);
	}

	public function userAlbum(Request $request){
		$id = $request->input('id');
		return view("family-tree/album-view")->with('profileId', $id);
	}

}