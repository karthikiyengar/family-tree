<?php namespace App\Http\Controllers\Business;

use App\Http\Controllers;
use App\Http\Requests;
use App\Http\Controllers\Controller;
use App\Http\Controllers\DAL\DALController;
use Illuminate\Http\Request;
use Everyman\Neo4j\Cypher\Query;


class EventController extends Controller {
	
	# 1. Add created by, venue fields to events

	public function addNewEvent(Request $request) {
		$eventName = $request -> input('eventName');
		$eventDescription = $request -> input('eventDescription');
		$eventDate = $request -> input('eventDate');
		$eventVenue = $request -> input('eventVenue');
		$eventCreatedBy = \Session::get('fullName');
		$client = DALController::getConnection();
		$newEvent = $client->makeNode();
		$newEvent->setProperty('name', $eventName)
		->setProperty('description', $eventDescription)
    	->setProperty('date', $eventDate)
    	->setProperty('adminApproval', false)
    	->setProperty('venue', $eventVenue)
    		->save();

    	$eventLabel = $client->makeLabel('Event');
    	$newEvent->addLabels(array($eventLabel));

    	$node = $client->getNode(\Session::get('id'));
    	$node->relateTo($newEvent, 'Owner')
    		->save();
        return \Redirect::to('events')->with('status', 'Event added succesfully. Waiting for approval');
	}

	public function addNewEventAdmin(Request $request) {
		$eventName = $request -> input('eventName');
		$eventDescription = $request -> input('eventDescription');
		$eventDate = $request -> input('eventDate');
		$eventVenue = $request -> input('eventVenue');
		$eventCreatedBy = \Session::get('fullName');

		$client = DALController::getConnection();
		$newEvent = $client->makeNode();
		$newEvent->setProperty('name', $eventName)
		->setProperty('description', $eventDescription)
    	->setProperty('date', $eventDate)
    	->setProperty('adminApproval', true)
    	->setProperty('venue', $eventVenue)
    	->setProperty('createdBy', $eventCreatedBy)
    	->save();

    	$eventLabel = $client->makeLabel('Event');
    	$newEvent->addLabels(array($eventLabel));

    	$node = $client->getNode(\Session::get('id'));
    	$node->relateTo($newEvent, 'Owner')
    		->save();
        return \Redirect::to('/admin/forms/add-events')->with('status', 'Event added succesfully.');
	}

	# 2. Write  logic to fetch events created by the person in the event page and provide delete functionality

	public function getUserEvents(Request $request) {
		
		$createdById = $request->input('id');
		$client = DALController::getConnection();
		$queryString = "MATCH (user:User)-[r:Owner]->(event:Event) WHERE ID(user)=$createdById RETURN event,user";
        $transaction = $client->beginTransaction();
        $query = new Query($client, $queryString);
        $result = $transaction->addStatements($query);
        $transaction->commit();
      
 		$jsonArray = array();
        for ($i = 0; $i < $result->count(); $i++) 
        {
           $jsonArray[$i] = array('id' => $result[$i]['event'] -> getId(), 
           	'eventName' => $result[$i]['event']->getProperty('name'),
	        'eventDescription' => $result[$i]['event'] ->getProperty('description'),
	        'eventDate' => $result[$i]['event'] -> getProperty('date'),
            'eventVenue' => $result[$i]['event'] -> getProperty('venue'),
            'adminApproval' => $result[$i]['event'] -> getProperty('adminApproval'),
           	'createdBy' => $result[$i]['user'] -> getProperty('fullName'));
        }
        return $jsonArray;

	}

	

	public function deleteEvent($id){
		$client = DALController::getConnection();
		$queryString = "MATCH (event:Event)<-[r]->() WHERE ID(event)=$id DELETE event,r";
        $transaction = $client->beginTransaction();
        $query = new Query($client, $queryString);
        $result = $transaction->addStatements($query);
        $transaction->commit();
	}



	public function deleteUserEvent(Request $request) {
		
		
		//Check if node is created by current user here for safety.

		$id = $request->input('id');
		$this->deleteEvent($id);
		return \Redirect::to('events')->with('status', 'Node Deleted Successfully');
	}

	public function deleteUserEventAdmin(Request $request) {			
		//Check if node is created by current user here for safety.

		$id = $request->input('id');
		$this->deleteEvent($id);
		return \Redirect::to('/admin/forms/add-events')->with('status', 'Node Deleted Successfully');
	}
	

	# 3. Add functionality to add comments to a particular event (commentedBy, commentMessage, timestamp)
	
	public function addComment(Request $request) {
		$client = DALController::getConnection();
		$eventId = $request -> input('eventId');
		$commentedBy = $request -> input('commentedBy');
		$commentMessage = addslashes($request -> input('commentMessage'));
		$commentMessage = trim(htmlspecialchars($commentMessage));
		$commentMessage = substr($commentMessage, 0, 200);
		$queryString = "MATCH (n) WHERE ID(n)=$eventId MATCH (m) WHERE ID(m)=$commentedBy CREATE (m)-[r:comment{commentMessage:'$commentMessage', time:timestamp()}]->(n)";
        $transaction = $client->beginTransaction();
        $query = new Query($client, $queryString);
        $result = $transaction->addStatements($query);
        $transaction->commit();
     	return \Redirect::to('home');
	}

	public function addCommentAdmin(Request $request) {
		$client = DALController::getConnection();
		$eventId = $request -> input('eventId');
		$commentedBy = $request -> input('commentedBy');
		$commentMessage = addslashes($request -> input('commentMessage'));
		$commentMessage = trim(htmlspecialchars($commentMessage));
		$commentMessage = substr($commentMessage, 0, 200);
		$queryString = "MATCH (n) WHERE ID(n)=$eventId MATCH (m) WHERE ID(m)=$commentedBy CREATE (m)-[r:comment{commentMessage:'$commentMessage', time:timestamp()}]->(n)";
        $transaction = $client->beginTransaction();
        $query = new Query($client, $queryString);
        $result = $transaction->addStatements($query);
        $transaction->commit();
     	return \Redirect::to('/admin/events');
	}

	public function deleteComment($commentId,$commentedBy) 
    {
        /*
        */
        //var reject = true;
        $client = DALController::getConnection();
        $queryString = "MATCH (n:User)-[r:comment]->(m:Event) WHERE ID(n) = $commentedBy and ID(r) = $commentId DELETE r";
        $transaction = $client->beginTransaction();
        $query = new Query($client, $queryString);
        $result = $transaction->addStatements($query);
        $transaction->commit();
        return \Redirect::to('/');
    }



	# 4. Write logic to get all active events for home page. (Get name and profile pic of commenter as well)
	# 5. Write logic to get all those comments for that event along with profile pic and name of commenter by date

	public function getEvents(Request $request) {
		
		$client = DALController::getConnection();
		
		$queryString = "MATCH (event:Event)<-[r:Owner]-(creator) WHERE event.adminApproval=true RETURN DISTINCT event, creator";
        
        $transaction = $client->beginTransaction();
        $query = new Query($client, $queryString);
        $resultEvents = $transaction->addStatements($query);
        $transaction->commit();

		$commentsArray = array();
		$jsonArray = array();
		$countEvents = count($resultEvents);
		for ($i = 0; $i < $countEvents; $i++) {
			$eventArray = array();
			$queryString = "MATCH (user:User) -[r:comment]-> (event:Event) WHERE ID(event)=" . $resultEvents[$i]['event']->getId() . " RETURN DISTINCT user, r";
    	    $transaction2 = $client->beginTransaction();
        	$query2 = new Query($client, $queryString);
        	$resultComments = $transaction2->addStatements($query2);
        	$transaction2->commit();	
        	$countComments = count($resultComments); 
        	$commentsArray = array();
        	for ($j = 0; $j < $countComments; $j++) {
        		$timestamp = ($resultComments[$j]['r']->getProperty('time'))/1000;
        		date_default_timezone_set('Asia/Kolkata');
        		$humantime = date('d-M-y, H:m', $timestamp);
        		$commentsArray[] = array('commentMessage'=>$resultComments[$j]['r']->getProperty('commentMessage')
        								,'timestamp'=>$humantime
        								,'commentedBy'=>$resultComments[$j]['user']->getId()
        								,'commentedByName'=>$resultComments[$j]['user']->getProperty('fullName')
        								,'profileImage'=>$resultComments[$j]['user']->getProperty('profileImage')
        								,'commentId'=>$resultComments[$j]['r']->getId()
									);
			}
			
			$jsonArray[] = array('creatorName'=>$resultEvents[$i]['creator']->getProperty('fullName')
			,'creatorProfileImage'=>$resultEvents[$i]['creator']->getProperty('profileImage')
			,'eventName'=> $resultEvents[$i]['event']->getProperty('name')
			,'eventDescription' => $resultEvents[$i]['event']->getProperty('description')
			,'eventDate' =>$resultEvents[$i]['event']->getProperty('date')
			,'eventVenue' =>$resultEvents[$i]['event']->getProperty('venue')
			,'eventId' =>$resultEvents[$i]['event']->getId()
			,'comments' => $commentsArray);
 		} 		
 		return $jsonArray;
	}	
}
