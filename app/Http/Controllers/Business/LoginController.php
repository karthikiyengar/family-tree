<?php namespace App\Http\Controllers\Business;


use App\Http\Controllers\Controller;
use App\Http\Controllers\DAL\DALController;
use App\Http\Controllers\Service\RESTService;
use Everyman\Neo4j\Cypher\Query;
use Illuminate\Http\Request;

class LoginController extends Controller
{
    public function login(Request $request)
    {

        $txtPhone =trim(htmlspecialchars($request->input('txtPhone'),ENT_QUOTES | ENT_HTML5, 'UTF-8'));
        $txtPin =trim(htmlspecialchars($request->input('txtPin'),ENT_QUOTES | ENT_HTML5, 'UTF-8'));
        
        $client = DALController::getConnection();
        $queryString = "MATCH (n {mobileNumber: '$txtPhone', generatedPin: $txtPin}) RETURN n";
        $transaction = $client->beginTransaction();
        $query = new Query($client, $queryString);
        $result = $transaction->addStatements($query);
        $transaction->commit();
        if ($transaction->isClosed()) {
            if ($result->count() == 1) {
                $id = $result[0]['x']->getId();
                $fullName = $result[0]['x'] -> getProperty('fullName');
                $profileImage = $result[0]['x'] -> getProperty('profileImage');
                $adminApproval = $result[0]['x'] -> getProperty('adminApproval');
                $isAdmin = $result[0]['x'] -> getProperty('isAdmin');
                \Session::put('mobileNumber', $txtPhone);
                \Session::put('id', $id);	
                \Session::put('parent_id', $id);
                \Session::put('parentFullName', $fullName);
                \Session::put('isAdmin',$isAdmin);
                if($profileImage=='')
                {
                    \Session::put('profileImage', 'ui-sam.jpg');
                }
                else
                {
                    \Session::put('profileImage', $profileImage);
                }
                \Session::put('parentProfileImage', $profileImage);
                \Session::put('fullName', $fullName);
                \Session::put('adminApproval',$adminApproval);
                return \Redirect::to('/')->withFlashMessage('Logged in Successfully.');
            }       
            else {
                \Session::flash("status","Invalid mobile number or pin.");
                return \Redirect::to('/login');
            }
        }
        else {
            \Session::flash("status","Server error, Please try again.");
            return \Redirect::to('/login')->withFlashMessage("Server Error.");
        }
    }

    public function loginAdmin(Request $request)
    {

        $txtPhone =trim(htmlspecialchars($request->input('txtPhone'),ENT_QUOTES | ENT_HTML5, 'UTF-8'));
        $txtPin =trim(htmlspecialchars($request->input('txtPin'),ENT_QUOTES | ENT_HTML5, 'UTF-8'));
        
        $client = DALController::getConnection();
        $queryString = "MATCH (n {mobileNumber: '$txtPhone', generatedPin: $txtPin}) WHERE n.isAdmin=true RETURN n";
        $transaction = $client->beginTransaction();
        $query = new Query($client, $queryString);
        $result = $transaction->addStatements($query);
        $transaction->commit();
        if ($transaction->isClosed()) {
            if ($result->count() == 1) {
                $id = $result[0]['x']->getId();
                $fullName = $result[0]['x'] -> getProperty('fullName');
                $profileImage = $result[0]['x'] -> getProperty('profileImage');
                $adminApproval = $result[0]['x'] -> getProperty('adminApproval');
                $isAdmin = $result[0]['x'] -> getProperty('isAdmin');
                \Session::put('mobileNumber', $txtPhone);
                \Session::put('id', $id);   
                \Session::put('parent_id', $id);
                \Session::put('parentFullName', $fullName);
                \Session::put('isAdmin',$isAdmin);
                if($profileImage=='')
                {
                    \Session::put('profileImage', 'ui-sam.jpg');
                }
                else
                {
                    \Session::put('profileImage', $profileImage);
                }
                \Session::put('parentProfileImage', $profileImage);
                \Session::put('fullName', $fullName);
                \Session::put('adminApproval',$adminApproval);
                return \Redirect::to('/admin')->withFlashMessage('Logged in Successfully.');
            }       
            else {
                \Session::flash("status","Invalid mobile number or pin.");
                return \Redirect::to('/admin/login');
            }
        }
        else {
            \Session::flash("status","Server error, Please try again.");
            return \Redirect::to('/admin/login')->withFlashMessage("Server Error.");
        }
    }


    public function profileChange(Request $request) 
    {
        $parentId = \Session::get('parent_id');
        $deadId = $request->input('id');
        $client = DALController::getConnection();
        $currentNode = $client -> getNode($parentId);
        if  ($currentNode != null) {
            $ownerRelation = $currentNode->getRelationships(array('Owner'));
            $outgoingNodes = array_map(function ($rel) {
                return $rel->getEndNode();
            }, $ownerRelation);
            array_push($outgoingNodes, $currentNode);
            $count = count($outgoingNodes);
            if ($count != 0) {
                $flag = false;
                for($i = 0; $i < $count; $i++) {
                    if ($deadId == $outgoingNodes[$i]->getId()) {
                        \Session::put('id', $deadId);            
                        \Session::put('fullName', $outgoingNodes[$i]->getProperty('fullName'));
                        if($outgoingNodes[$i]->getProperty('profileImage')==''){
                            \Session::put('profileImage', 'ui-sam.jpg');
                        } else{
                            \Session::put('profileImage', $outgoingNodes[$i]->getProperty('profileImage'));
                        }
                        $flag = true;
                    }
                }
                if ($flag == false) {
                    \Session::put('id', $parentId);            
                    \Session::put('fullName', $currentNode ->getProperty('fullName'));
                    if($outgoingNodes[$i]->getProperty('profileImage')==''){
                        \Session::put('profileImage', 'ui-sam.jpg');
                    } else{
                        \Session::put('profileImage', $outgoingNodes[$i]->getProperty('profileImage'));
                    }
                }
            }
            else {
                \Session::put('id', $parentId);            
                \Session::put('fullName', $currentNode ->getProperty('fullName'));
                if($outgoingNodes[$i]->getProperty('profileImage')==''){
                    \Session::put('profileImage', 'ui-sam.jpg');
                } else{
                    \Session::put('profileImage', $outgoingNodes[$i]->getProperty('profileImage'));
                }
            } 
        }
    }

    public function getPin(Request $request)
    {

        /*
         * This controller will be called when the customer clicks on 'Get Pin'. This will generate a new PIN
         * and create a new node in the database, or update it if it already exists.
         */

        $txtPhone =trim(htmlspecialchars($request->input('txtPhone'),ENT_QUOTES | ENT_HTML5, 'UTF-8'));
        if($txtPhone!="" && strlen($txtPhone)==10)
        {
            $generatedPin = rand(10000, 99999);
            
            /* Send SMS */
            $receivers = array();
            $receivers[] = $txtPhone;
            $message = 'Your PIN is ' . $generatedPin . '. Please login to the family tree website.';
            RESTService::sendSms($receivers, $message);

            $client = DALController::getConnection();
            $queryString="match (n:invitedUser{mobileNumber:'$txtPhone'}) return n";
            
            $transaction = $client->beginTransaction();
            $query = new Query($client, $queryString);
            $result = $transaction->addStatements($query);
            $transaction->commit();

            if ($transaction->isClosed()) {
                if ($result->count() == 1) {
                    $updateNodeLabel="MATCH (n:invitedUser{mobileNumber:'$txtPhone'}) REMOVE n:invitedUser SET n:User";
                    $mergeNodes = "MATCH (n:User {mobileNumber: '" . $txtPhone . "'}) SET n.generatedPin = " . $generatedPin . ",n.adminApproval=false,n.profileImage='ui-sam.jpg'";
                    $transaction = $client->beginTransaction();
                    $updateNode = new Query($client, $updateNodeLabel);
                    $mergeNode = new Query($client, $mergeNodes);
                    $result1 = $transaction->addStatements($updateNode);
                    $result1 = $transaction->addStatements($mergeNode);
                    $transaction->commit();
                }
                else
                {

                    $queryString="match (n:User{mobileNumber:'$txtPhone'}) return n";
                    
                    $transaction = $client->beginTransaction();
                    $query = new Query($client, $queryString);
                    $result = $transaction->addStatements($query);
                    $transaction->commit();

                    if ($transaction->isClosed()) {
                        if ($result->count() == 1) {
                            $transaction1 = $client->beginTransaction();
                            $queryString2 = "MERGE (n:User {mobileNumber: '" . $txtPhone . "'}) SET n.generatedPin = " . $generatedPin;
                            #$transaction2 = $client->beginTransaction();
                            $query2 = new Query($client, $queryString2);
                            $result2 = $transaction1->addStatements($query2);
                            $transaction1->commit();
                        }
                        else
                        {
                            $transaction1 = $client->beginTransaction();
                            $queryString2 = "MERGE (n:User {mobileNumber: '" . $txtPhone . "'}) SET n.generatedPin = " . $generatedPin . ",n.adminApproval=false";
                            #$transaction2 = $client->beginTransaction();
                            $query2 = new Query($client, $queryString2);
                            $result2 = $transaction1->addStatements($query2);
                            $transaction1->commit();
                        }
                    }

                   
                }
                return json_encode(array("status"=>"ok"));
            }
        }
        else
        {
            if($txtPhone=="")
                return json_encode(array("status"=>"empty"));
            else if(strlen($txtPhone)!=10)
                return json_encode(array("status"=>"count"));
        }
        
    }

    public function logout()
    {
        \Session::forget('mobileNumber');
        \Session::forget('id');
        \Session::forget('parent_id');
        \Session::forget('fullName');
        return \Redirect::to('login');
    }
    public function logoutAdmin()
    {
        \Session::forget('mobileNumber');
        \Session::forget('id');
        \Session::forget('parent_id');
        \Session::forget('fullName');
        return \Redirect::to('/admin/login');
    }
}
