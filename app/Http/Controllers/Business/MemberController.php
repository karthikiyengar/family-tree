<?php
namespace App\Http\Controllers\Business;

use App\Http\Controllers\Controller;
use App\Http\Controllers\DAL\DALController;
use App\Http\Controllers\Service\RESTService;
use Everyman\Neo4j\Cypher\Query;
use Everyman\Neo4j\Path;
use Everyman\Neo4j\Relationship;
use Illuminate\Http\Request;

class MemberController extends Controller
{

    public static function createRelation($relationship,$node,$newNode,$client){
        if ($relationship == 'children') {
        
            $gender = $node->getProperty('gender');
            if ($gender == 'Male') {
                
                #Check if spouse is present for current node, if yes, add her as mother.

                $relationships = $node->getRelationships(array('spouse'));
                $spouseNode = array_map(function ($rel) {
                    return $rel->getEndNode();  
                }, $relationships);

                $count = count($spouseNode); 

                if (count($spouseNode) == 1) 
                {
                    $spouseNode[0]->relateTo($newNode, 'mother')->setProperty('adminApproval', false)->save();
                    $node->relateTo($newNode, 'father')->setProperty('adminApproval', false)->save();
                }
                else 
                {
                    $node->relateTo($newNode, 'father')->setProperty('adminApproval', false)->save();
                }
                
            }
            if ($gender == 'Female') {

                #Check if spouse is present for current node, if yes, add him as father.

                $relationships = $node->getRelationships(array('spouse'));
                $spouseNode = array_map(function ($rel) {
                    return $rel->getEndNode();  
                }, $relationships);

                $count = count($spouseNode); 

                if (count($spouseNode) == 1) 
                {
                    $spouseNode[0]->relateTo($newNode, 'father')->setProperty('adminApproval', false)->save();
                    $node->relateTo($newNode, 'mother')->setProperty('adminApproval', false)->save();
                }
                else 
                {
                    $node->relateTo($newNode, 'mother')->setProperty('adminApproval', false)->save();
                }
                
            }
        }
        
        if ($relationship == 'father') {

            $relationships = $node->getRelationships(array('mother'), Relationship::DirectionIn);
            $motherNode = array_map(function ($rel) {
                return $rel->getStartNode();  
            }, $relationships);

            $count = count($motherNode); 
            
            #Also create a spouse relationship if mother exists
            if (count($motherNode) == 1) 
            {
                $newNode->relateTo($motherNode[0], 'spouse')->setProperty('adminApproval', false)->save();
                $newNode->relateTo($node, 'father')->setProperty('adminApproval', false)->save();
            }
            else 
            {
                $newNode->relateTo($node, 'father')->setProperty('adminApproval', false)->save();
            }
        }
        
        if ($relationship == 'mother') {

            $relationships = $node->getRelationships(array('father'), Relationship::DirectionIn);
            $fatherNode = array_map(function ($rel) {
                return $rel->getStartNode();  
            }, $relationships);

            $count = count($fatherNode); 
            if (count($fatherNode) == 1) 
            {
                
                $fatherNode[0]->relateTo($newNode, 'spouse')->setProperty('adminApproval', false)->save();
                $newNode->relateTo($node, 'mother')->setProperty('adminApproval', false)->save();
            }
            else 
            {
                $newNode->relateTo($node, $relationship)->setProperty('adminApproval', false)->save();
            }
        }
        

        if ($relationship == 'spouse') {
            $gender = $node->getProperty('gender');

            #Add Wife: If current user is male, find all children (Outgoing father relationships)
            
            if ($gender == 'Male') 
            {
                $relationships = $node->getRelationships(array('father'), Relationship::DirectionOut);
                $childNodes = array_map(function ($rel) {
                return $rel->getEndNode();  
                }, $relationships);


                #Add all existing children with wife node, having mother relationship.

                for ($i = 0; $i < count($childNodes); $i++) {
                    $newNode->relateTo($childNodes[$i], 'mother')->setProperty('adminApproval', false)->save();
                }

                $node->relateTo($newNode, 'spouse')->setProperty('adminApproval', false)->save();

            }

            #Add Husband: If current user is female, find all children (Outgoing mother relationships)
            
            if ($gender == 'Female') {
                $relationships = $node->getRelationships(array('mother'), Relationship::DirectionOut);
                $childNodes = array_map(function ($rel) {
                return $rel->getEndNode();  
                }, $relationships);

                #Add all existing children with wife node, having mother relationship.

                for ($i = 0; $i < count($childNodes); $i++) {
                    $newNode->relateTo($childNodes[$i], 'father')->setProperty('adminApproval', false)->save();
                }

                $newNode->relateTo($node, 'spouse')->setProperty('adminApproval', false)->save();
            }
        }
    }

    public function inviteUser(Request $request)
    {
        
        #Fetching data from Invite form input fields
        
        $firstName    = trim(htmlspecialchars($request->input('firstName'), ENT_QUOTES | ENT_HTML5, 'UTF-8'));
        $lastName     = trim(htmlspecialchars($request->input('lastName'), ENT_QUOTES | ENT_HTML5, 'UTF-8'));
        $mobile       = trim(htmlspecialchars($request->input('mobile'), ENT_QUOTES | ENT_HTML5, 'UTF-8'));
        $emailID      = trim(htmlspecialchars($request->input('emailID'), ENT_QUOTES | ENT_HTML5, 'UTF-8'));
        $nodeRelation = trim(htmlspecialchars($request->input('nodeRelation'), ENT_QUOTES | ENT_HTML5, 'UTF-8'));
        $relationship = trim(htmlspecialchars($request->input('relationship'), ENT_QUOTES | ENT_HTML5, 'UTF-8'));
        $fullName     = \Session::get("parentFullName");
        #Sending SMS to invited user
        if ($mobile != "") {
            $message = "You have been by " . $fullName . ". Please login to Family Tree Site";
            $receivers = array();
            $receivers[] = $mobile; 
            RESTService::sendSms($receivers, $message);
        }
        
        
        #fetching mobile number from session of current logged-in user
        
        $mobileNumber = \Session::get('mobileNumber');
        
        #connecting to DB and create query and insert new node
        
        
        $client = DALController::getConnection();
        
        $currentId = \Session::get('id');
        $node = $client->getNode($currentId);
        $newNode = $client->makeNode();
        $newNode->setProperty('firstName', $firstName);
        $newNode->setProperty('lastName', $lastName);
        $newNode->setProperty('mobileNumber', $mobile);
        $newNode->setProperty('email', $emailID);
        $newNode->setProperty('profileImage', 'ui-sam.jpg');
        $newNode->setProperty('fullName', $firstName." ".$lastName);
        $newNode->save();



        $userLabel = $client->makeLabel('invitedUser');
        $labels = $newNode->addLabels(array($userLabel));
        $newNodeId = $newNode->getId();

        //call function
        MemberController::createRelation($relationship,$node,$newNode,$client);

        return \Redirect::to('/add-members')->with('status', 'Member Added Successfully');      
    }


    public function addExistingMember(Request $request)
    {
        $nodeRelation = $request->input('existingNodeRelation');
        $relationship = $request->input('existingRelationship');
        $targetUserID = $request->input('targetUserID');
        $mobileNumber = \Session::get('mobileNumber');
        
        $client = DALController::getConnection();
        
        $currentId = \Session::get('id');
        $node = $client->getNode($currentId);
        $targetUserNode = $client->getNode($targetUserID);
        $gender = $node->getProperty('gender');

        MemberController::createRelation($relationship,$node,$targetUserNode,$client);
         /*   if ($gender == 'Male') {
                $node->relateTo($newNode, 'father')->setProperty('adminApproval', false)->save();
            }
            if ($gender == 'Female') {
                $node->relateTo($newNode, 'mother')->setProperty('adminApproval', false)->save();
            }*/
         return \Redirect::to('/add-members')->with('status', 'Member Added Successfully');

    }
    
    //Relatives
    public function getRelatives(Request $request)
    {
        $jsonArray = array();
        if (\Session::get('id')) {
            $id          = \Session::get('id');
            $client      = DALController::getConnection();
            /* $queryString = 'MATCH (n)<-[r]->(m) WHERE ID(n)=13 AND NOT (type(r)="Owner" OR type(r)="belong" OR type(r)="albums" OR type(r)="Event") return distinct type(r), n,m,type(r)'; */
            $queryString = "MATCH (n:User)-[r]->(m) WHERE ID(n)=$id AND NOT type(r)='Owner' AND type(r) IN ['father','mother'] return distinct m, 'children' as relation 
                            UNION 
                            MATCH (n:User)-[r]->(m) WHERE ID(n)=$id AND NOT type(r)='Owner' AND type(r) = 'spouse' 
                            RETURN distinct m, 'spouse' as relation 
                            UNION 
                            MATCH (n:User)<-[r]-(m) 
                            WHERE ID(n)=$id AND NOT type(r)='Owner' 
                            return distinct m, type(r) as relation";
            $transaction = $client->beginTransaction();
            $query       = new Query($client, $queryString);
            $result      = $transaction->addStatements($query);
            $transaction->commit();
            
            for ($i = 0; $i < $result->count(); $i++) {
                $jsonArray[$i] = array(
                    'id' => $result[$i]['m']->getId(),
                    'relativeType' => $result[$i]['relation'],
                    'relativeFullName' => $result[$i]['m']->getProperty('fullName')
                );
            }
        } else {
            $jsonArray[] = array(
                "errorMessage" => "You dont have admin rights to access this file."
            );
        }
        return $jsonArray;
    }

    public static function getRelationString($startNode, $endNode) 
    {
        $client = DALController::getConnection();
        /*$startNode = $request->input('startNode');
        $endNode = $request->input('endNode');*/

        if(!empty($startNode) && is_numeric($startNode)  && !empty($endNode) and is_numeric($endNode))
        {

            $queryString = "START startNode=node($startNode), endNode=node($endNode)
                            MATCH paths=shortestPath(startNode<-[:mother|father|spouse*]->endNode)
                            RETURN EXTRACT(node in NODES(paths) | node.fullName) as relatedNodes, EXTRACT(node in RELS(paths) | type(node)) as relations";
            $transaction = $client->beginTransaction();
            $query       = new Query($client, $queryString);
            $result      = $transaction->addStatements($query);
            $transaction->commit();
            if ($transaction->isClosed()) {
                if (count($result) > 0) {
                    $nodes = $result[0]['relatedNodes'];
                    $rels = $result[0]['relations'];
                    $relationString = '';
                    for ($i = 0; $i < count($nodes) - 1; $i++)
                    {
                        $relationString = $relationString . $nodes[$i] . ' is the ' . $rels[$i] . ' of ' . $nodes[$i+1] . '. ';
                    }
                    return $relationString;
                }
                else if ($startNode == $endNode)
                {
                    return 'This is you';
                }
                else
                {
                    return 'You are not related with this person.';
                }
            }
        }
        else {
            return 'Invalid input';
        }

    }
}