<?php
namespace App\Http\Controllers\Business;

use App\Http\Controllers\Controller;
use App\Http\Controllers\DAL\DALController;
use App\Http\Controllers\Business\AlbumController;
use App\Http\Requests;
use Everyman\Neo4j\Cypher\Query;
use Everyman\Neo4j\Path;
use Illuminate\Http\Request;


class ProfileController extends Controller
{
    
    public static $map = array('aboutMe','fullName', 'gender', 'maritialStatus', 'profileImage', 'firstName', 'lastName', 'dateOfBirth','dateOfDeath', 'village', 'mobileNumber','email', 'currentArea', 'currentCity', 'bloodGroup', 'educationLevel', 'educationSpecialization', 'occupation', 'mothersName', 'fathersName', 'mothersMothersName', 'mothersFathersVillage', 'mothersMothersVillage', 'mothersFathersName', 'fathersFathersName', 'fathersMothersName');
    

    public function submitform(Request $request)
    {
        
        /*
         * Get all POST Parameters
         */
        $requestArray = $request->all();
        $client       = DALController::getConnection();
        
        /* Condition 1: Update own profile */
        
        if ($request->input('type') == null) {
            $id   = \Session::get('id');
            $user = $client->getNode($id);
            foreach ($requestArray as $key => $value) {
                if (in_array($key, self::$map) && $key != 'mobileNumber' && strlen($value) < 160) {
                    $user->setProperty($key, trim(htmlspecialchars($value)));
                }
            }
            
            $user->removeProperty('_token');
            $fullName = htmlspecialchars($requestArray['firstName']) . ' ' . htmlspecialchars($requestArray['lastName']);
            $user->setProperty('fullName', $fullName);
            \Session::put('fullName', $fullName);
            #uploading Profile picture
            if ($request->hasFile('profileImage')) {
                $image           = $request->file('profileImage');
                $filename        = \Session::get('id') . "_" . \Session::get('fullName') . "." . $image->getClientOriginalExtension();
                $destinationPath = "profilepics/";
                $fullPath = $destinationPath.$filename;
                $image->move($destinationPath, $filename);
                AlbumController::compress_image($fullPath, $fullPath, 75);

                $user->setProperty('profileImage', $filename);
                
                \Session::put('profileImage', $filename);
            }
            $user->save();
            return view('family-tree/forms/profile-form')->with('status', 'Form submitted successfully');
        }
        
        /* Condition 2: Add Dead Member */
        $type = $request->input('type');
        if ($type == 'dead') {
            $relationship = $request->input('relationship');
            $phoneNumber  = session('mobileNumber');
            
            $node        = \Session::get('id');
            $currentNode = $client->getNode($node);
            
            //makenode -> father as user node, set dead flag is true, set adminApproval as false
            $newNode = $client->makeNode();
            foreach ($requestArray as $key => $value) {
                if (in_array($key, self::$map)) {
                    $newNode->setProperty($key, $value);
                }
            }
            
            $newNode->removeProperty('_token');
            $fullName = $requestArray['firstName'] . ' ' . $requestArray['lastName'];
            $newNode->setProperty('fullName', $fullName);
            $newNode->setProperty('adminApproval', false);
            $newNode->setProperty('isDead', true);
            $newNode->setProperty('profileImage', 'ui-sam.jpg');
            $newNode->save();
            
            $userLabel = $client->makeLabel('User');
            $labels    = $newNode->addLabels(array(
                $userLabel
            ));

            MemberController::createRelation($relationship,$currentNode,$newNode,$client);
            $currentNode->relateTo($newNode, 'Owner')->setProperty('adminApproval', false)->save();

        }
        
        return \Redirect::to('/add-members')->with('status','Dead member has been added and waiting for admin approval.');
    }
    
    public function getTopHeader()
    {
        #Gets Logout Dropdown with owned Members list.

        $client      = DALController::getConnection();
        $parentId    = \Session::get('parent_id');
        $queryString = "match (n) -[r:Owner*]-> (m:User) where m.adminApproval=true AND ID(n)=$parentId return m";
        $transaction = $client->beginTransaction();
        $query       = new Query($client, $queryString);
        $result      = $transaction->addStatements($query);
        $transaction->commit();

        $jsonArray = array();       
        for($i = 0; $i < $result->count(); $i++) {
            $jsonArray[$i]['id'] =  $result[$i][0]->getId();
            if($result[$i][0]->getProperty('profileImage')==''){
                $jsonArray[$i]['profileImage']= 'ui-sam.jpg';
            }else{
                $jsonArray[$i]['profileImage']= $result[$i][0]->getProperty('profileImage');
            }
            $jsonArray[$i]['fullName']= $result[$i][0]->getProperty('fullName');
        }        
        return $jsonArray;
    }
    public function details(Request $request)
    {
        #Provide profile detail of a member
        
        $id = $request->input('id');
        if (empty($id)) 
        {
            $id = \Session::get('id');
        }
        if (!empty($id)) {
            $client      = DALController::getConnection();
            $currentNode = $client->getNode($id);
            $jsonArray   = array();
            
            foreach (self::$map as $element) {
                $jsonArray[$element] = $currentNode->getProperty($element);
            }

            $jsonArray['relationString'] = MemberController::getRelationString(\Session::get('id'), $id);
            return $jsonArray;
        }
    }
    
    
    
    public function search(Request $request)
    {
        $txtSearch   = $request->get('query');
        $type   = $request->get('type');
        $currentId = \Session::get('id');
        $client      = DALController::getConnection();
        if (empty($type)) {
            $transaction = $client->beginTransaction();
            $queryString = "MATCH (n:User) WHERE n.adminApproval = true RETURN (n) ";
            $query       = new Query($client, $queryString);
            $result      = $transaction->addStatements($query);
            $transaction->commit();
        }
        if ($type = 'existing') {
            $transaction = $client->beginTransaction();
            /* Change Query Here */
            $queryString = "START unrelatedNode = node(*), currentUser = node($currentId)
                            WHERE not ((unrelatedNode)-[*]-(currentUser))
                            AND NOT ID(unrelatedNode) = $currentId 
                            AND unrelatedNode.adminApproval=true
                            return unrelatedNode";
            $query       = new Query($client, $queryString);
            $result      = $transaction->addStatements($query);
            $transaction->commit();   
        }
        if ($transaction->isClosed()) {
            for ($i = 0; $i < $result->count(); $i++) {
                $jsonArray[$i] = array(
                    'value' => $result[$i]['n']->getProperty('firstName') . ' ' . $result[$i]['n']->getProperty('lastName'),
                    'data' => 'http://localhost:8000/profile/',
                    'id' => $result[$i]['n']->getId(),
                    'profileImage' => $result[$i]['n']->getProperty('profileImage')
                );
            }
        }
        #$suggestions['suggestions'] = $jsonArray;
        return $jsonArray;
    }

    public function searchAdmin(Request $request)
    {
        $txtSearch   = $request->get('query');
        $client      = DALController::getConnection();
        $transaction = $client->beginTransaction();
        #$queryString = "MATCH(n:User) WHERE n.firstName =~ '(?i)$txtSearch.*' or n.lastName =~ '(?i)$txtSearch.*' or n.fullName =~ '(?i)$txtSearch.*' return(n)";
        $queryString = "MATCH (n:User) RETURN (n)";
        $query       = new Query($client, $queryString);
        $result      = $transaction->addStatements($query);
        $transaction->commit();
        if ($transaction->isClosed()) {
            for ($i = 0; $i < $result->count(); $i++) {
                $jsonArray[$i] = array(
                    'value' => $result[$i]['n']->getProperty('firstName') . ' ' . $result[$i]['n']->getProperty('lastName'),
                    'data' => 'http://localhost:8000/admin/',
                    'id' => $result[$i]['n']->getId(),
                    'profileImage' => $result[$i]['n']->getProperty('profileImage')
                );
            }
        }
        #$suggestions['suggestions'] = $jsonArray;
        return $jsonArray;
    }
}
