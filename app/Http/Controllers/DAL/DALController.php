<?php namespace App\Http\Controllers\DAL;

use App\Http\Controllers\Controller;
use App\Http\Requests;
use Everyman\Neo4j\Client;

class DALController extends Controller
{

    private static $client;

    /*
     * Creating a singleton factory to maintain one connection object. Connection pooling can be done if required.
     */


    public static function getConnection()
    {
        if (!(self::$client)) {
            self::$client = new Client('54.148.71.114', 7474);

            self::$client->getTransport()
                ->setAuth('neo4j', 'bpk123');
        }
        return self::$client;
    }
}
