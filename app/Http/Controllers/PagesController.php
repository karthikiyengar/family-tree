<?php namespace App\Http\Controllers;

use App\Http\Requests;
use App\Http\Controllers\DAL\DALController;
use Everyman\Neo4j\Cypher\Query;

class PagesController extends Controller
{

    public function profile()
    {
        if(\Session::has('mobileNumber')){
            if (!(\Session::get('adminApproval'))) {
                return view('family-tree/forms/profile-form')->with('status', 'You have not been approved by admin yet.');
            }
            else {
                return view('family-tree/forms/profile-form')->with('status','');
            }
        }
        else{
            return \Redirect::to('/login');
            // return 'Else';
            // \Redirect::to('\login');
        }
    }

    public function inviteDeadUser($member) {
        if(\Session::has('mobileNumber')){
            if(\Session::get('adminApproval')){
                return view('family-tree/forms/add-dead-form')->with('member', $member);
            }   
            else{
                return \Redirect::to('/profile')->with('status', 'You have not been approved by admin yet.');
            }
        }
        else{
            return \Redirect::to('/login');
            // return 'Else';
            // \Redirect::to('\login');
        }
    }

    
    public function home()
    {
        if(\Session::has('mobileNumber')){
            if(\Session::get('adminApproval')){
                return view('family-tree/home');
            }   
            else{
                return \Redirect::to('/profile')->with('status', 'You have not been approved by admin yet.');
            }
        }
        else{
            return \Redirect::to('/login');
            // return 'Else';
            // \Redirect::to('\login');
        }
    }


    public function mytree()
    {
        if(\Session::has('mobileNumber')){
            if(\Session::get('adminApproval')){
                return view('family-tree/my-tree');
            }   
            else{
                    return \Redirect::to('/profile')->with('status', 'You have not been approved by admin yet.');
            }
        }
        else{
            return \Redirect::to('/login');
        }
    }


    public function ancestorView()
    {
        if(\Session::has('mobileNumber')){
            if(\Session::get('adminApproval')){
                $client = DALController::getConnection();
                $id    = \Session::get('id');
                $queryString = "MATCH (y:User)<-[r:father*]-(root:User) WHERE NOT (root)<-[r:father*]-() AND ID(y)=$id RETURN root";
                $transaction = $client->beginTransaction();
                $query       = new Query($client, $queryString);
                $result      = $transaction->addStatements($query);
                $transaction->commit();
                if (count($result) > 0) {
                    return view('family-tree/ancestor-view')->with('ancestorId', $result[0]['root']->getId());
                }
                else {
                    return view('family-tree/ancestor-view')->with('ancestorId', $id);   
                }
            }   
            else{
                    return \Redirect::to('/profile')->with('status', 'You have not been approved by admin yet.');
            }
        }
        else{
            return \Redirect::to('/login');
            // return 'Else';
            // \Redirect::to('\login');
        }
    }


    public function ancestortree()
    {
        if(\Session::has('mobileNumber')){
            if(\Session::get('adminApproval')){
                return view('family-tree/ancestor-tree');
            }   
            else{
                    return \Redirect::to('/profile')->with('status', 'You have not been approved by admin yet.');
            }
        }
        else{
            return \Redirect::to('/login');
            // return 'Else';
            // \Redirect::to('\login');
        }
    }

    public function events()
    {
        if(\Session::has('mobileNumber')){
            if(\Session::get('adminApproval')){
                return view('family-tree/events');
            }   
            else{
                    return \Redirect::to('/profile')->with('status', 'You have not been approved by admin yet.');
            }
        }
        else{
            return \Redirect::to('/login');
            // return 'Else';
            // \Redirect::to('\login');
        }
    }

    public function addmembers()
    {
        if(\Session::has('mobileNumber')){
            if(\Session::get('adminApproval')){
                return view('family-tree/add-members') -> with('status', '');
            }   
            else{
                    return \Redirect::to('/profile')->with('status', 'You have not been approved by admin yet.');
            }
        }
        else{
            return \Redirect::to('/login');
            // return 'Else';
            // \Redirect::to('\login');
        }
    }

    public function login()
    {

        if(\Session::has('mobileNumber')){
            if(\Session::get('adminApproval')){
                return \Redirect::to('/home');
            }   
            else{
                return \Redirect::to('/profile')->with('status', 'You have not been approved by admin yet.');
            }
        }
        else{
            return view('family-tree/login');
        }
    }

    public function albums()
    {
        if(\Session::has('mobileNumber')){
            if(\Session::get('adminApproval')){
                return view('family-tree/albums');
            }   
            else{
                    return \Redirect::to('/profile')->with('status', 'You have not been approved by admin yet.');
            }
        }
        else{
            return \Redirect::to('/login');
            // return 'Else';
            // \Redirect::to('\login');
        }
    }

     public function viewProfile($id) {
        
        /*$client = DALController::getConnection();
        $nodeStart = $client->getNode(28);
        $nodeEnd = $client -> getNode(2);
        $path =  $nodeStart->findPathsTo($nodeEnd)->setMaxDepth(8)->getSinglePath();
        $pathArray = array();
        $relArray = array();
        foreach ($path as $node) {
            $pathArray[] = $node->getId();
        }
        $path->setContext(Path::ContextRelationship);
        foreach ($path as $rel) {
            $relArray[] =  $rel->getType();
        }
        $finalArray = array_merge($relArray, $pathArray);
        //return $finalArray;*/
        if(\Session::has('mobileNumber')){
            if(\Session::get('adminApproval')){
                return view('family-tree/profile')->with('profileId', $id);
            }
            else{
                return \Redirect::to('/profile')->with('status', 'You have not been approved by admin yet.');
            }
        }
        else {
            return \Redirect::to('/login');
        }
    }

}
