<?php
namespace App\Http\Controllers\Service;

use App\Http\Requests;
use App\Http\Controllers\Controller;
use App\Http\Controllers\DAL\DALController;
use Everyman\Neo4j\Cypher\Query;
use Illuminate\Http\Request;

class RESTService extends Controller
{
    public function sendMessage (Request $request) {
        $mobileNumbers = $request->input('mobileNumber');
        $txtMessage = $request->input('message');
        self::sendSms($mobileNumbers, $txtMessage);
    }

    public function getContacts(Request $request) 
    {
        $id = $request->input('id');
        if (empty($id)) {
            $id = \Session::get('id');
        }
        if (!empty($id) && is_numeric($id))  
        {
            $client      = DALController::getConnection();
            $transaction = $client->beginTransaction();
            $queryString = "MATCH (currentUser:User)<-[r*0..3]->(relatedUser:User) WHERE ID(currentUser) = $id AND NOT relatedUser.mobileNumber IS NULL RETURN DISTINCT ID(relatedUser), relatedUser.mobileNumber, relatedUser.fullName";
            $query       = new Query($client, $queryString);
            $result      = $transaction->addStatements($query);
            $transaction->commit();

            $resultArray = array();
            for ($i = 0; $i < count($result); $i++) {
                $contact = array('relatedUserId' => $result[$i]['relatedUser.id'], 'fullName' => $result[$i]['relatedUser.fullName'], 'mobileNumber' => $result[$i]['relatedUser.mobileNumber']);
                $resultArray[] = $contact;                
            }
        }
        return $resultArray;
    }
    
    
    public static function sendSms($receivers, $message)
    {
            $receiversString = rtrim(implode(',', $receivers), ',');
            $message = urlencode($message);
            if (strlen($message) > 0 && count($receivers) > 0) {
                \Unirest\Request::verifyPeer(false);
                $uri = "http://www.k3digitalmedia.in/sendsms/bulksms.php?username=K3sharp&password=tech&type=TEXT&sender=FMTREE&mobile=" . $receiversString . "&message=" . $message;
                //echo $uri;
                $response = \Unirest\Request::get($uri);
                return $response;
            }
            else {
                return 'Invalid Parameters';
            }
    }
    public function getSingleLevel(Request $request)
    {
        $targetId = $request->input('id');
        $isChildRequest = $request->input('child');
        if (!empty($targetId)) {
            $client      = DALController::getConnection();
            $transaction = $client->beginTransaction();
            $queryString = "MATCH (currentUser:User) -[:father|mother]-> (relatedUser:User) WHERE ID(currentUser) = $targetId RETURN DISTINCT relatedUser, 'child' as relation, 'outgoing' as direction UNION
                            MATCH (currentUser:User) <-[:spouse]-> (relatedUser:User) WHERE ID(currentUser) = $targetId RETURN DISTINCT relatedUser, 'spouse' as relation, 'outgoing' as direction UNION
                            MATCH (currentUser:User) <-[r:father|mother]- (relatedUser:User) WHERE ID(currentUser) = $targetId RETURN DISTINCT relatedUser, type(r) as relation, 'incoming' as direction";
            $query       = new Query($client, $queryString);
            $result      = $transaction->addStatements($query);
            $transaction->commit();
            
            $nodesArray = array();
            $edgesArray = array();
            
            $count = count($result);
            
            # Add Session Node to the Array if first request
            if (!$isChildRequest) {
                $currentUserId           = \Session::get('id');
                $currentUserName         = \Session::get('fullName');
                $currentUserProfileImage = '/profilepics/' . \Session::get('profileImage');
            }

            # Add Reuqested Node to the Array if not first request

            else {
                $node                    = $client->getNode($targetId);
                $currentUserId           = $targetId;
                $currentUserName         = $node->getProperty('fullName');
                $currentUserProfileImage = '/profilepics/' . $node->getProperty('profileImage');
            }

            $nodesArray[] = array(
                'data' => array(
                    'id' => $currentUserName,
                    'uid' => $currentUserId
                ),
                'css' => array(
                    'content' => $currentUserName,
                    'background-image' => $currentUserProfileImage
                )
            );
            
            
            for ($i = 0; $i < $count; $i++) {
                
                # Add Connected Nodes 
                
                $relatedUserId           = $result[$i]['relatedUser']->getId();
                $relatedUserName         = $result[$i]['relatedUser']->getProperty('fullName');
                $relatedUserProfileImage = '/profilepics/' . $result[$i]['relatedUser']->getProperty('profileImage');
                $nodesArray[]            = array(
                    'data' => array(
                        'id' => $relatedUserName,
                        'uid' => $relatedUserId
                    ),
                    'css' => array(
                        'content' => $relatedUserName,
                        'background-image' => $relatedUserProfileImage
                    )
                );
                
                # Generate Edges
                
                $relationType = $result[$i]['relation'];
                $relationDirection = $result[$i]['direction'];
                
                if ($relationDirection == 'outgoing') {
                    $edgesArray[] = array(
                        'data' => array(
                            'source' => $currentUserName,
                            'target' => $relatedUserName
                        ),
                        'css' => array(
                            'content' => $relationType
                        )
                    );
                }
                
                if ($relationDirection == 'incoming') {
                    $edgesArray[] = array(
                        'data' => array(
                            'source' => $relatedUserName,
                            'target' => $currentUserName
                        ),
                        'css' => array(
                            'content' => $relationType
                        )
                    );
                }
            }
            
            return json_encode(array(
                'nodes' => $nodesArray,
                'edges' => $edgesArray
            ));

        } else {
            return 'Invalid POST';
        }
    }
}
