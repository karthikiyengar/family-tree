<?php

/*
|--------------------------------------------------------------------------
| Application Routes
|--------------------------------------------------------------------------
|
| Here is where you can register all of the routes for an application.
| It's a breeze. Simply tell Laravel the URIs it should respond to
| and give it the controller to call when that URI is requested.
|
*/

/*
 * TODO Check CSRF tokens in forms
 */

/*
 * Misc
 */

Route::get('/', 'PagesController@home');
Route::get('/home', 'PagesController@home');
Route::get('getrelationstring', 'Business\MemberController@getRelationString');
Route::get('ancestor-view', 'PagesController@ancestorView');
Route::get('getcontacts', 'Service\RESTService@getContacts');
#For Home
Route::post('sendmessage', 'Service\RESTService@sendMessage');

/*
 * Login
 */

Route::get('login', 'PagesController@login');
Route::get('logout', 'Business\LoginController@logout');
Route::post('login', 'Business\LoginController@login');
Route::post('getpin', 'Business\LoginController@getPin');


#Change owners for dead
Route::post('profile-change', 'Business\LoginController@profileChange');

/*
 * Profile Details
 */ 

Route::post('/profile/details', 'Business\ProfileController@details');
Route::get('/profile', 'PagesController@profile');
Route::post('/profile', 'Business\ProfileController@submitform');
Route::get('/profile/search', 'Business\ProfileController@search');
Route::get('/profile/{id}', 'PagesController@viewProfile');
Route::get('/admin/profile/{id}', 'PagesController@viewProfile');
Route::get('/admin/search', 'Business\ProfileController@searchAdmin');

Route::get('my-tree', 'PagesController@mytree');
Route::get('ancestor-tree', 'PagesController@ancestortree');
Route::get('add-members', 'PagesController@addmembers');
Route::get('events', 'PagesController@events');

/*
 * Add Members
 */

Route::post('add-members/inviteUser','Business\MemberController@inviteUser');
Route::post('add-members/addExistingMember', 'Business\MemberController@addExistingMember');
Route::get('add-members/{member}/dead','PagesController@inviteDeadUser');
Route::post('add-members/{member}/dead','Business\ProfileController@submitform');

/*
*	AlbumUpload
*/
Route::get('albums', 'PagesController@albums');
Route::get('album/userAlbum', 'Business\AlbumController@userAlbum');
Route::get('/admin/albums', 'AdminController@albumsadmin');
Route::post('album/photoUpload', 'Business\AlbumController@photoUpload');
Route::post('/admin/album/photoUpload', 'Business\AlbumController@photoUploadAdmin');
Route::post('album/loadPhotos', 'Business\AlbumController@loadPhotos');
Route::post('/admin/album/loadPhotos', 'Business\AlbumController@loadPhotosAdmin');
Route::get('album/del', 'Business\AlbumController@deleteImage');
Route::get('/admin/album/del', 'Business\AlbumController@deleteImageAdmin');

/*
 * Admin Route
 */

Route::get('/admin/logout', 'Business\LoginController@logoutAdmin');
Route::post('/admin/login', 'Business\LoginController@loginAdmin');
Route::get('/admin', 'AdminController@homeadmin');
Route::get('/admin/forms/add-events', 'AdminController@addeventsadmin');
Route::get('/admin/forms/add-users', 'AdminController@addusersadmin');
Route::get('/admin/home', 'AdminController@homeadmin');
Route::get('/admin/login', 'AdminController@loginAdmin');
Route::get('/admin/master', 'AdminController@masteradmin');
Route::get('/admin/albums', 'AdminController@albumsadmin');
Route::get('/admin/users-tree', 'AdminController@userstreeadmin');

Route::get('/admin/views/view-events', 'AdminController@vieweventsadmin');
Route::get('/admin/views/view-events-request', 'AdminController@vieweventsrequestadmin');
Route::get('/admin/views/view-users', 'AdminController@viewusersadmin');
Route::get('/admin/views/view-users-request', 'AdminController@viewusersrequestadmin');
Route::get('/admin/views/view-users-request/view-details/{id}', 'Admin\UserRequestController@viewDetailsUserRequest');
Route::get('/admin/views/view-users-request/accept/{id}', 'Admin\UserRequestController@acceptRequest');
Route::get('/admin/views/view-events-request/acceptEvent/{id},{createdById}', 'Admin\UserRequestController@acceptRequestEvent');
Route::get('/admin/views/view-users-request/add-admin-access/{id}', 'Admin\UserRequestController@addAdminAccess');
Route::get('/admin/views/view-users-request/delete/{id},{nodeType}', 'Admin\UserRequestController@deleteRequest');
Route::get('/admin/views/view-users-request/remove-admin-access/{id}', 'Admin\UserRequestController@removeAdminAccess');

/*
 * REST API
 */

Route::post('admin/GetUsersRequests', 'Admin\UserRequestController@getUsersRequests');
Route::post('admin/GetUsersView', 'Admin\UserRequestController@getUsersView');
Route::post('admin/GetEventsRequests', 'Admin\UserRequestController@getEventsRequests');
Route::post('GetTopHeader', 'Business\ProfileController@getTopHeader');
Route::post('GetRelatives', 'Business\MemberController@getRelatives');
Route::post('getsinglelevel', 'Service\RESTService@getSingleLevel');

/*
 * Events
 */

Route::post('addnewevent', 'Business\EventController@addNewEvent');
Route::post('/admin/addnewevent', 'Business\EventController@addNewEventAdmin');
Route::post('/admin/getuserevents', 'Business\EventController@getUserEvents');
Route::post('getuserevents', 'Business\EventController@getUserEvents');
Route::post('getrelatives', 'Business\MemberController@getRelatives');
Route::post('deleteuserevents', 'Business\EventController@deleteUserEvent');
Route::post('/admin/deleteuserevents', 'Business\EventController@deleteUserEventAdmin');
Route::get('getevents', 'Business\EventController@getEvents');
Route::get('/admin/views/getevents', 'Business\EventController@getEvents');
Route::post('addComment', 'Business\EventController@addComment');
Route::post('/addComment/addComment', 'Business\EventController@addCommentAdmin');
Route::get('/deleteComment/{commentId},{commentedBy}', 'Business\EventController@deleteComment');