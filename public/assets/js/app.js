// Place third party dependencies in the lib folder
//
// Configure loading modules from the lib directory,
// except 'app' ones, 
requirejs.config({
    "baseUrl": "/assets/js",
    "paths": {
        "parsley":"lib/parsley.min",
		"handlebars": "lib/handlebars",
        "jquery": "lib/jquery",
        "jquery.loadJSON": "lib/jquery.loadJSON",
        "cytoscape": "lib/cytoscape",
        "common-scripts": "common-scripts",
        "calendar": "calendar",
        "bootstrap": "lib/bootstrap.min",
        "jquery-ui": "lib/jquery-ui",
        "jquery.accordion": "lib/jquery.accordion",
        "jquery.nicescroll": "lib/jquery.nicescroll",
        "jquery.scrollTo": "lib/jquery.scrollTo.min",
        "jquery.zabuto": "lib/jquery.zabuto",
        "jquery.backstretch": "lib/jquery.backstretch",
        "jquery.autocomplete": "lib/jquery.autocomplete",
        "jquery.migrate": "lib/jquery.migrate",
        "demo.preview": "lib/demo.preview",
        "dmuploader": "lib/dmuploader",
        "login": "login",
        "glisse":"lib/glisse"
    },
    "shim": {
        "bootstrap": ["jquery"],
        "jquery.loadJSON": ["jquery"],
        "jquery.backstretch": ["jquery"],
        "jquery.accordion": ["jquery"],
        "jquery.nicescroll": ["jquery"],
        "jquery-ui": ["jquery"],
        "jquery.scrollTo": ["jquery"],
        "jquery.zabuto": ["jquery"],
        "common-scripts": ["jquery.accordion", "jquery.nicescroll", "jquery.scrollTo"],
        "calendar": ["jquery.zabuto", "bootstrap"],
        "jquery.autocomplete": ["jquery"],
        "jquery.migrate":  ["jquery"],
        "demo.preview":  ["jquery"],
        "dmuploader":  ["jquery"],
        "glisse": ["jquery"]
    },
    waitSeconds: 0
});

// Load the main app module to start the app
//requirejs(["main"]);
