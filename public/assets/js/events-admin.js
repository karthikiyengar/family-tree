require(['jquery'], function($) {
    $(document).ready(function() {
        $("#btnAddEvent").click(function() {
            $("#addEventDiv").toggle();
        });

        $('body').on('click', '.btn-delete', function (){
            var id = $(this).attr('name');
            var jsonData = {};
            jsonData.id = id;
            $.ajax({
                url:'/admin/deleteuserevents',
                data: jsonData,
                type: 'POST',           
                success: function(data) {
                    window.location = '/admin/forms/add-events';
                },
                failure: function() {
                    alert('Failed to delete data.')
                }
            })
        });



        $.ajaxSetup({
            headers: {
                'X-CSRF-Token': $('meta[name=_token]').attr('content')
            }
        });
        var strId = $('meta[name=id]').attr('content');
        jsonData = {
            'id': strId
        };


        $.ajax({
            url: '/admin/getuserevents',
            data: jsonData,
            method: 'post',
            beforeSend: function() {
                $("#spinner").show();
            },
            success: function(data) {
                if (data.length === 0) {
                    currentEventsHtml = '<div class="form-panel">No events present currently</div>'
                } else {
                    var currentEventsHtml = '';
                    $.each(data, function(index) {
                        currentEventsHtml += '<div class="form-panel">'
                        currentEventsHtml += '<h2>' + data[index].eventName + '</h2>';
                        currentEventsHtml += '<b>Event Name: </b>' + data[index].eventDescription + '<br/>'
                        currentEventsHtml += '<b>Event Description: </b>' + data[index].eventDescription + '<br/>'
                        currentEventsHtml += '<b>Event Date: </b>' + data[index].eventDate + '<br/>'
                        currentEventsHtml += '<b>Created by: </b>' + data[index].createdBy + '<br/>'
                        currentEventsHtml += '<b>Status: </b>' + getApprovalMessage(data[index].adminApproval) + '<br/>'
                        currentEventsHtml += '<button type="button" class="btn btn-large btn-danger btn-delete" name="' + data[index].id + '">Delete</button>' + '<br/>'
                        currentEventsHtml += '</div>';
                    })
                }



                $("#spinner").hide();
                $("#currentEvents").html(currentEventsHtml);
            },
            failure: function(data) {
                alert('Failed to get events');
            }
        })

        var getApprovalMessage = function(status) {
            return status ? 'Event approved by administrator' : 'Event pending approval'
        }
    })

})