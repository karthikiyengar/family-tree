require(['jquery','parsley', 'calendar'], function($) {
	$.ajaxSetup({
        headers: {
            'X-CSRF-Token': $('meta[name=_token]').attr('content')
        }
    });
    $("#spinner").fadeIn();
	$.ajax({
		url: 'profile/details/',
		type: 'post',
		success: function (data) {
			$.each(data, function (key, value) {
				if (key != 'profileImage') 
				{
					$('#' + key).val(value);
				}
			})
			$("#spinner").hide();
			$("#main-form").fadeIn();
		},
		failure: function () {
			console.log('Failed to get details');
		}
	})
});