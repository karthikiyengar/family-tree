require(['jquery'], function($) {
    var eventHtml = '';
    var commentHtml = '';
    window.$ = $;
    $.getJSON('/admin/getevents').done(function(data) {
      eventHtml+="<div class='col-md-6 col-md-offset-1'>";
        $.each(data, function(index) {
            eventHtml +=
                " <div class='row bdbg'>"+
                "   <div class='col-md-4'>"+
                "     <div class='row'>"+
                "       <div class='col-md-3'><a href='profile.html'><img src='/profilepics/" + data[index][0].creatorProfileImage + "' class='img-circle' width='50'></a></div>"+
                "       <div class='col-md-9'><p class='para'>" + data[index][0].creatorName + "</p></div>"+
                "     </div>"+
                "   </div>"+
                "   <div class='col-md-8'>"+
                "     <div class='row'>"+
                "       <div class='col-md-12'>"+
                "         <h4 class='heading'>" + data[index][0].eventName + "</h4>"+
                "       </div>"+
                "       <div class='col-md-12'>"+
                "         <span class='para'>Venue :" + data[index][0].eventVenue + "</span>" + "<br/>" +
                "         <span class='para'>Date :" + data[index][0].eventDate + "</span>"+
                "       </div>"+
                "     </div>  "+
                
                "</div>" +
                " <div class='col-md-12'>" +
                "   <div class='show-comments' name=" + data[index][0].eventId + "><span class='glyphicon glyphicon-comment'> Toggle Comments</div>" +
                " </div>" +
                "   <div class='row'>"+
                "     <div class='col-md-12'>"+
                "     </div></div>" +
                "   <div class='comment-section comment-section-" + data[index][0].eventId + " style='display:none'>";

            $.each(data[index][0].comments, function(commentIndex) {
                eventHtml +=
                    
                    "   <div class='row'>"+
                    "     <div class='col-md-1'><img src='/profilepics/" + data[index][0].comments[commentIndex].profileImage + "' class='img-circle' width='80'>"+
                    "     </div>"+
                    "     <div class='col-md-11'>"+
                    "       <div class='row'>"+
                    "         <div class='col-md-12'><span class='para'>" + data[index][0].comments[commentIndex].commentedByName + " - </span><span class='dtl'>" + data[index][0].comments[commentIndex].commentMessage + "</span>&nbsp;&nbsp;&nbsp;&nbsp;<a href='/deleteComment/"+data[index][0].comments[commentIndex].commentId+","+data[index][0].comments[commentIndex].commentedBy+"'>x</a>"+
                    "         </div>"+
                    "         <div class='col-md-12'><span class='para'>" + "Comment on : " + data[index][0].comments[commentIndex].timestamp + "</span>" +
                    "         </div>"+
                    "       </div>"+
                    "       <hr>"+
                    "     </div>"+
                    "   </div>";
            });
        
            eventHtml +=
                "   </div>" +      
                "   <div class='row'>"+
                "     <div class='col-md-1'><img src='assets/img/ui-sam.jpg' class='img-circle' width='30'>"+
                "     </div>"+
                "     <form method='POST' action='/addComment'><div class='col-md-8'><input type='text' class='form-control' name='commentMessage' placeholder='Comment'>"+
                "     </div>"+
                "     <div class='col-md-3'><button type='submit' class='btn btn-theme' style='margin:0px;'>Comment</button>"+
                "     <input type='hidden' name='eventId' value=" + data[index][0].eventId  + ">" +
                "     <input type='hidden' name='commentedBy' value=" + $('meta[name=id]').attr('content') + ">" +
                "     <input type='hidden' name='_token' value=" + $('meta[name=_token]').attr('content') + ">" +
                "     </div></form>"+
                "   </div>"+
                "</div>"
                

        })
        $('body').on('click', '.show-comments', function (event){
            var selectedId = $(this).attr('name');
            $('.comment-section-' + selectedId).fadeToggle();
        });
        $('#mainContentAdmin').html(eventHtml);
        $('.comment-section').hide();
    });
    
    
});


/*

[
  [
    {
      "eventName": "Test!@",
      "eventDescription": "",
      "eventDate": "",
      "eventVenue": "",
      "comments": []
    }
  ],
  [
    {
      "eventName": "Test!@",
      "eventDescription": "",
      "eventDate": "",
      "eventVenue": "",
      "comments": []
    }
  ],
  [
    {
      "eventName": "Test2",
      "eventDescription": "AS",
      "eventDate": "2015-03-18",
      "eventVenue": "DSAD",
      "comments": []
    }
  ],
  [
    {
      "eventName": "Test Event",
      "eventDescription": "!",
      "eventDate": "",
      "eventVenue": "Mum",
      "comments": [
        {
          "commentMessage": "Abc",
          "timestamp": 1427546202449,
          "commentedBy": 13,
          "profileImage": "13_Bala .jpg"
        }
      ]
    }
  ]
]

*/