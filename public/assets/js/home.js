require(['jquery', 'handlebars', 'parsley'], function($, Handlebars) {
    $.ajaxSetup({
        headers: {
            'X-CSRF-Token': $('meta[name=_token]').attr('content')
        }
    });

    profileImage = $('meta[name=profileImage]').attr('content');
    var eventHtml = '';
    var commentHtml = '';
    var source = $("#entry-template").html();
    var template = Handlebars.compile(source);
    $("#spinner").fadeIn();
    $.getJSON('getevents').done(function(data) {
        var html = template(data);
        $("#main-content").html(html);
        $('body').on('click', '.show-comments', function(event) {
            var selectedId = $(this).attr('name');
            $('.comment-section-' + selectedId).fadeToggle();
        });
        commentedBy = $('meta[name=id]').attr('content');
        csrfToken = $('meta[name=_token]').attr('content');
        profileImage = $('meta[name=profileImage]').attr('content');
        $(".commentedBy").val(commentedBy);
        $(".csrfToken").val(csrfToken);
        $(".commentImg").attr('src', '/profilepics/' + profileImage);
        $(".form-comment").submit(function() {
            $(".btn-submit").attr('disabled','disabled');
        })
        $('#btnSendSms').on('click', function() {
            $.ajax({
                url: 'getcontacts',
                type: 'GET',
                success: function(data) {
                    var sendSmsHtml = '';
                    if (data.length === 0) {
                        sendSmsHtml = '';
                    } else {
                        $.each(data, function(index) {
                            sendSmsHtml += "<input type='checkbox' value='" + data[index].mobileNumber + "'/>&nbsp;" + data[index].fullName + " - " + data[index].mobileNumber + "<br/>";
                        })
                    }
                    $('#reciever').html(sendSmsHtml);

                    $('#btnModalSend').click(function() {

                        var selectedNumbers = $("#sendSmsModal input:checkbox:checked").map(function() {
                            return $(this).val();
                        }).get();
                        var message = $("#txtMessage").val();
                        $.post('sendmessage', {
                            'mobileNumber': selectedNumbers,
                            'message': message
                        }).done(function(data) {
                            console.log('Server Says: ' + data);
                        })

                        $('#sendSmsModal').hide();
                    })
                },
                failure: function() {
                    alert('Failed to get reciever.');
                }
            });
        });
        $('.comment-section').hide();
        $("#spinner").hide();
    });
});