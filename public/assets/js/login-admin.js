require(['jquery', 'jquery.backstretch','parsley'], function ($) {
	$(document).ready(function () {
		$.backstretch("/assets/img/login-bg.jpg", {speed: 500});

		$.ajaxSetup({
			headers: {'X-CSRF-Token': $('meta[name=_token]').attr('content')}
		});
	});
});

