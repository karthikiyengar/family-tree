require(['jquery', 'jquery.backstretch','parsley'], function ($) {
        $(document).ready(function () {
            $.backstretch("assets/img/login-bg.jpg", {speed: 500});

            $.ajaxSetup({
                headers: {'X-CSRF-Token': $('meta[name=_token]').attr('content')}
            });

            //after clicking on get pin this ajax is called.
            $('#getPin').click(function (event) {
                //$('#form-login').parsley().validate();
                if($('#form-login').parsley().validate('block1'))
                {
                    var txtPhone = $('#txtPhone').val();
                    //alert("Please enter valid number.");
                    $.ajax({
                        url: 'getpin',
                        type: 'post',
                        data: {
                            "txtPhone": txtPhone
                        },
                        beforeSend: function() {
                             $("#getPin").val("Sending sms.....");
                             $("#getPin").attr("disabled", true);
                         },
                        success: function (data) {
                            jsonData = JSON.parse(data);

                            if(jsonData['status']=='ok')
                                alert('PIN sent to your phone.Please check it.');
                            else if(jsonData['status']=='count')
                                alert('Please check your mobile number is 10 digit.');
                            else if(jsonData['status']=='empty')
                                alert('PLease enter valid mobile number.');
                            else
                                alert("Please try again.");
                           $("#getPin").val("Get Pin");
                           $("#getPin").attr("disabled", false);
                        },
                        failure: function () {
                            alert('Please try again.');
                        }
                    });
                }
            });

            /*// $("#txtPhone,#txtPin").keydown(function(event){
            //      $("#txtPhone,#txtPin").css("background-color", "");
            //     if(!/\d/.test(String.fromCharCode(event.keyCode))){
            //          $("#txtPhone,#txtPin").css("background-color", "red");
            //           return /\d/.test(String.fromCharCode(event.keyCode));
            //     }
            // });

             //called when key is pressed in textbox
            $("#txtPhone,#txtPin").keypress(function (e) {
                $("#txtPhone,#txtPin").css("border-color", "");
                //if the letter is not digit then display error and don't type anything
                if (e.which != 8 && e.which != 0 && (e.which < 48 || e.which > 57)) {
                //display error message
                    $("#txtPhone,#txtPin").css("border-color", "red");
                    return false;
                }
            });*/

           /* $("#btnLogin").click(function(){
                if($("#txtPhone,#txtPin").val()=="" || $("#txtPhone").val().length!=10 || $("#txtPin").val().length!=5){
                    //alert($("#txtPhone").val().length);
                    alert("Please enter mobile number and pin.");
                    return false;
                }
            });*/
        });
    });
