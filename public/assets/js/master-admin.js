require(['jquery', 'common-scripts', 'jquery.autocomplete', 'bootstrap'], function($) {
    $.ajaxSetup({
        headers: {
            'X-CSRF-Token': $('meta[name=_token]').attr('content')
        }
    });

    var firstSearchFlag = true;
    $("#txtSearch,#txtExistSearch").focus(function() {
        console.log(firstSearchFlag);
        if (firstSearchFlag) {
            $.getJSON('/admin/search').success(function(data) {
                $('#txtSearch,#txtExistSearch').autocomplete({
                    lookup: data,
                    onSelect: function(suggestion) {
                        id = suggestion.id;
                        window.location = '/admin/forms/add-users/?id='+ id;
                    }
                });
                firstSearchFlag = false;
            })
        }
    });
});