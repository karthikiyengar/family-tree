require(['jquery', 'common-scripts', 'jquery.autocomplete', 'bootstrap'], function($) {
    $.ajaxSetup({
        headers: {
            'X-CSRF-Token': $('meta[name=_token]').attr('content')
        }
    });

    var firstSearchFlag = true;
    $("#txtSearch").focus(function() {
        console.log(firstSearchFlag);
        if (firstSearchFlag) {
            $.getJSON('/profile/search').success(function(data) {
                $('#txtSearch').autocomplete({
                    lookup: data,
                    onSelect: function(suggestion) {
                        id = suggestion.id;
                        window.location = '/profile/' + id;
                    }
                });
                firstSearchFlag = false;
            })
        }
    });


    var firstSearchFlagForExisting = true;
    $("#txtExistSearch").focus(function() {
        console.log(firstSearchFlagForExisting);
        if (firstSearchFlagForExisting) {
            $.getJSON('/profile/search', {type:'existing'}).success(function(data) {
                $('#txtExistSearch').autocomplete({
                    lookup: data,
                    onSelect: function(suggestion) {
                        id = suggestion.id;
                        $("#targetUserID").val(id);
                    }
                });
                firstSearchFlagForExisting = false;
            })
        }
    });
    profileHtml = '';


    $.ajax({
        url: '/GetTopHeader',
        type: 'POST',
        success: function(data) {
            $.each(data, function(index) {
                profileHtml += '<li name="' + data[index].id + '" class="image-dropdown"> <img src="/profilepics/' + data[index].profileImage + '" width="20"/>' + data[index].fullName + '</li>';
            });
            cId = $('meta[name=id]').attr('content');
            pId = $('meta[name=parent_id]').attr('content');
            pFullName = $('meta[name=parentFullName]').attr('content');
            pProfileImage = $('meta[name=parentProfileImage]').attr('content');
            if (cId != pId) {
                profileHtml += '<li name="' + pId + '" class="image-dropdown"> <img src="/profilepics/' +
                    pProfileImage + '" width="20"/>' +
                    pFullName + '</li>';
            }


            profileHtml += '<a class="logout" href="/logout">Logout</a>';
            $("#profileList").html(profileHtml);
            $(".image-dropdown").on('click', function() {

                var id = $(this).attr('name');
                var jsonData = {};
                jsonData.id = id;
                $.ajax({
                    url: '/profile-change',
                    data: jsonData,
                    type: 'POST',
                    success: function(data) {
                        window.location = '/';
                    },
                    failure: function() {

                    }
                });
            });
            var selectedId = $('meta[name=id]').attr('content');
        },
        failure: function() {

        }
    });   
});
