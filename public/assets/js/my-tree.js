require(['jquery', 'cytoscape'], function($, cytoscape) {
    $.ajaxSetup({
        headers: {
            'X-CSRF-Token': $('meta[name=_token]').attr('content')
        }
    });
    var jsonData = [];
    var strId = $('meta[name=id]').attr('content');
    jsonData = {
        'id': strId
    };
    var firstCall;
    var makeJsonCall = function(jsonData) {
        firstCall = false;
        $.ajax({
            url: '/getsinglelevel',
            type: 'POST',
            data: jsonData,
            beforeSend: function() {
                $("#spinner").show();
            },
            success: function(data) {
                jsonData = JSON.parse(data);
                $("#cy").empty();
                $("#buttons").hide();
                var cy = cytoscape({
                    zoomingEnabled:false,
                    container: document.getElementById('cy'),
                    style: cytoscape.stylesheet()
                        .selector('node')
                        .css({
                            'height': 70,
                            'width': 70,
                            'shape': 'rectangle',
                            'background-fit': 'cover',
                            'border-color': '#000',
                            'border-width': 3,
                            'border-opacity': 0.5
                        })
                        .selector('edge')
                        .css({
                            'width': 2,
                            'target-arrow-shape': 'triangle',
                            'line-color': '#ffaaaa',
                            'target-arrow-color': '#ffaaaa'
                        }),
                    elements: jsonData,

                    layout: {
                        name: 'breadthfirst',
                        fit:true,
                        directed: true,
                        padding: 5,
                        animate:true
                    }
                }); // cy init

                cy.on('tap', 'node', function() {
                    var tapped = this;
                    window.tapped = tapped;
                    var tappedId = tapped.data().uid;
                    jsonData = {};
                    jsonData = {
                      'id': tappedId,
                      'child': true
                    };
                    $("#buttons").show();
                    
                    if (!firstCall) {
                        $("#btnHome").show();
                        $("#btnHome").off();
                        $("#btnHome").on('click', function() {
                            location.reload();
                        })
                    }
                    $("#btnViewProfile").off('click');
                    $("#btnViewProfile").on('click', function() {
                        window.location = '/profile/' + tappedId
                    })
                    $("#btnViewTree").off('click');
                    $("#btnViewTree").on('click',function() {
                        makeJsonCall(jsonData);    
                    });
                }); // on tap
                $("#spinner").hide();
            },
            failure: function() {
                alert('Single Level Tree Failed');
                $("#spinner").hide();
            }
        });
    }
    makeJsonCall(jsonData);
    firstCall = true;
});