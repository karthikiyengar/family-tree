require(['jquery','handlebars'], function($, Handlebars){
   $.ajaxSetup({
	    headers: {
	        'X-CSRF-Token': $('meta[name=_token]').attr('content')
	    }
    });
	var source   = $("#entry-template").html();
	var template = Handlebars.compile(source);	
	id = $('meta[name=profileId]').attr('content');
	
	$.post('/profile/details', { "id":id }).done(function(data){
		var html = template(data);
		$("#template-content").html(html); 	
	})
})
