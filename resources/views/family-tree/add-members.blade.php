@extends('family-tree/master')
@section('content')
    <!-- Bootstrap core CSS -->
    <link href="/assets/css/bootstrap.css" rel="stylesheet">
    <!--external css-->
    <link href="/assets/font-awesome/css/font-awesome.css" rel="stylesheet"/>
            <h3>Add Members</h3>
            <h5>{{$status}}</h5>
            <div id="spinner" style="display:none;text-align:center">
                <h5>Loading, Please Wait.<h5>
                <img src="/assets/img/spinner.gif" style="height:5%;width:5%">
            </div>
            <div class="row mt">

                <div id="add-member-content" style="display:none" class="col-lg-8 col-md-offset-1">
                    <div class="form-panel">

                        <form class="form-horizontal style-form">
							<input type="hidden" name="_token" value="{{{ csrf_token() }}}"/>
                            <div class="form-group">
                                <div class="col-sm-12">
                                    <button type="button" class="btn btn-theme member" id="father" name="father">Add
                                        Father
                                    </button>
                                    <div id="fatherHtml"></div>                                      
									<div> 
									</div>
                                </div>
                            </div>


                            <div class="form-group">
                                <div class="col-sm-12">
                                    <button type="button" id="mother" name="mother" class="btn btn-theme member">Add
                                        Mother
                                    </button>
                                    <div id="motherHtml"></div>
                                </div>

                            </div>

                            <div class="form-group">
                                <div class="col-sm-12">
                                    <button type="button" id="spouse" name="spouse" class="btn btn-theme member"> Add Spouse
                                    </button>
                                    <div id="spouseHtml"></div>
                                </div>
                            </div>


                            <div class="form-group">
                                <div class="col-sm-12">
                                    <button type="button" id="children" name="children" class="btn btn-theme member"> Add
                                        Children
                                    </button>
                                    <div id="childrenHtml"></div>
                                </div>
                            </div>


                        </form>
                    </div>
                </div>
                <!-- col-lg-12-->
            </div>

            <div id="AddOption" class="modal fade" tabindex='-1'>
                <div class="modal-dialog">
                    <div class="modal-content">
                        <div class="modal-header">
                            <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
                            <h4 class="modal-title">Add Member</h4>
                        </div>
                        <div class="modal-body">
                            <p>Please Click on any on options below:</p>
                            <button class="btn btn-theme" id="invite">Invite Member</button></p>
                            <button class="btn btn-theme"  id="exist" >Add Existing Member</button></p>
                            <a id="btnDead"><button class="btn btn-theme"  id="dead" >Add Dead Member</button></a>
                        </div>
                        <div class="modal-footer">
                            <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
                        </div>
                    </div>
                </div>
            </div>

            <!-- Invite Modal start-->
            <div id="inviteModal" class="modal fade" tabindex='-1'>
                            <div class="modal-dialog">
                                <div class="modal-content">
                                    <div class="modal-header">
                                        <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
                                        <h4 class="modal-title">Invite Member</h4>
                                    </div>
                                    <div class="modal-body">

                                        <p>Please fill in details of member:</p>
                                        <form class="form-horizontal style-form" action="add-members/inviteUser" method="post" id="invite-form" enctype="multipart/form-data" data-parsley-validate>
                                            <input type="hidden" name="_token" value="{{{ csrf_token() }}}" />
                                            <input type="hidden" id="nodeRelation" name="nodeRelation"/>
                                            <input type="hidden" id="relationship" name="relationship"/>
                                                <div class="form-group">
                                                    <label class="col-sm-3 col-sm-3 control-label">First Name :</label>
                                                    <div class="col-sm-9">
                                                        <input type="text" class="form-control" placeholder="First Name" id="firstName" name="firstName" required>
                                                    </div>
                                                </div>
                                                 <div class="form-group">
                                                    <label class="col-sm-3 col-sm-3 control-label">Last Name :</label>
                                                    <div class="col-sm-9">
                                                        <input type="text" class="form-control" placeholder="Last Name" id="lastName" name="lastName" required>
                                                    </div>
                                                </div>
                                                 <div class="form-group">
                                                    <label class="col-sm-3 col-sm-3 control-label">Mobile Number :</label>
                                                    <div class="col-sm-9">
                                                        <input type="text" class="form-control" placeholder="Mobile Number" id="mobile" name="mobile" data-parsley-pattern="^\d{10}$" required>
                                                    </div>
                                                </div>
                                                 <div class="form-group">
                                                    <label class="col-sm-3 col-sm-3 control-label">Email Id :</label>
                                                    <div class="col-sm-9">
                                                        <input type="email" class="form-control" placeholder="Email ID" id="emailID" name="emailID" required>
                                                    </div>
                                                </div>
                                                 <div class="form-group">
                                                     <label class="col-sm-3 col-sm-3 control-label"></label>
                                                    <div class="col-sm-9">
                                                        <p class=""><button class="btn btn-theme" type="submit"  id="sendInvitation" name="sendInvitation" >Send Invitation</button></p>
                                                    </div>
                                                </div>
                                        </form>
                                    </div>
                                    <div class="modal-footer">
                                        <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
                                    </div>
                                </div>
                            </div>
                        </div>
            <!-- end -->

            <!-- Existing Modal start-->
                        <div id="existModal" class="modal fade" tabindex='-1' data-parsley-validate >
                                        <div class="modal-dialog">
                                            <div class="modal-content">
                                                <div class="modal-header">
                                                    <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
                                                    <h4 class="modal-title">Invite Member</h4>
                                                </div>
                                                <div class="modal-body">

                                                    <p>Please fill in details of member:</p>
                                                    <form class="form-horizontal style-form" method="post" id="existingForm" action="add-members/addExistingMember" enctype="multipart/form-data">
                                                        <input type="hidden" name="_token" value="{{{ csrf_token() }}}" />
                                                        <input type="hidden" id="existingNodeRelation" name="existingNodeRelation"/>
                                                        <input type="hidden" id="existingRelationship" name="existingRelationship"/>
                                                        <input type="hidden" id="targetUserID" name="targetUserID"> 
                                                        <div class="form-group">
                                                                <div id="searchfield">
                                                                    <input id="txtExistSearch" type="text" class="form-control" placeholder="Search" name="q" required>
                                                                </div>
                                                            </div>
                                                            <div class="form-group">
                                                                <div id="outputbox">
                                                                    <p id="outputcontent"></p>
                                                                </div>
                                                            </div>
                                                             <div class="form-group">
                                                              <label class="col-sm-3 col-sm-3 control-label"></label>
                                                                 <div class="col-sm-9">
                                                                    <p class=""><button class="btn btn-theme"  id="addRelation" >Add Relation</button></p>
                                                                </div>
                                                            </div>
                                                    </form>
                                                </div>
                                                <div class="modal-footer">
                                                    <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                        <!-- end -->


                         <!-- Dead Modal start-->
                        <div id="deadModal" class="modal fade" tabindex='-1' data-parsley-validate>
                                        <div class="modal-dialog">
                                            <div class="modal-content">
                                                <div class="modal-header">
                                                    <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
                                                    <h4 class="modal-title">Invite Member</h4>
                                                </div>
                                                <div class="modal-body">

                                                    <p>Please fill in details of member:</p>
                                                    <form class="form-horizontal style-form" method="post" id="existingForm" action="add-members/addExistingMember" enctype="multipart/form-data">
                                                        <input type="hidden" name="_token" value="{{{ csrf_token() }}}" />
                                                        <input type="hidden" id="deadNodeRelation" name="deadNodeRelation"/>
                                                        <input type="hidden" id="deadRelationship" name="deadRelationship"/>                                                        
                                                        <div class="form-group">
                                                            <label class="col-sm-3 col-sm-3 control-label">First Name :</label>
                                                            <div class="col-sm-9">
                                                                <input type="text" class="form-control" placeholder="First Name" id="firstName" name="firstName" required>
                                                            </div>
                                                        </div>
                                                        <div class="form-group">
                                                            <label class="col-sm-3 col-sm-3 control-label">Last Name :</label>
                                                            <div class="col-sm-9">
                                                                <input type="text" class="form-control" placeholder="Last Name" id="lastName" name="lastName" required>
                                                            </div>
                                                        </div>
                                                         <div class="form-group">
                                                          <label class="col-sm-3 col-sm-3 control-label"></label>
                                                             <div class="col-sm-9">
                                                                <p class=""><button class="btn btn-theme"  id="addRelation" >Add Relation</button></p>
                                                            </div>
                                                        </div>
                                                    </form>
                                                </div>
                                                <div class="modal-footer">
                                                    <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                      
@stop
<!-- js placed at the end of the document so the pages load faster -->
<script src="/assets/js/lib/require.js"></script>
<script src="/assets/js/app.js"></script>



<!--BACKSTRETCH-->
<!-- You can use an image of whatever size. This script will stretch to fit in any screen size.-->
<!-- <script type="text/javascript" src="assets/js/jquery.backstretch.min.js"></script> -->
@section('scripts')
<script>
    require(['jquery', 'jquery.backstretch','bootstrap','parsley'], function ($) {
		
		$.ajaxSetup({
        headers: {'X-CSRF-Token': $('meta[name=_token]').attr('content')}
    }); 
    var strId = $('meta[name=id]').attr('content');
        jsonData = {
            'id': strId
        };
        var fatherHtml = '';
        var motherHtml = '';
        var spouseHtml = '';
        var childrenHtml = '';
         $.ajax({
            url: 'getrelatives',
            data: jsonData,
            method: 'post',
            beforeSend: function() {
                $("#add-member-content").hide();
                $("#spinner").show();
            },
            success: function(data) {
                if (data.length === 0) {
                     fatherHtml = '';
                     motherHtml = '';
                     spouseHtml = '';
                     childrenHtml = '';
                } 
                else {
                    
                    $.each(data, function(index) {     

                        if(data[index].relativeType=="father"){
                            fatherHtml += '<b>' + data[index].relativeFullName + '</b>&nbsp;&nbsp;';
                        }
                        if(data[index].relativeType=="mother"){
                            motherHtml += '<b>' + data[index].relativeFullName + '</b>&nbsp;&nbsp;';
                        }
                        if(data[index].relativeType=="spouse"){
                            spouseHtml += '<b>' + data[index].relativeFullName + '</b>&nbsp;&nbsp;';
                        }
                        if(data[index].relativeType=="children"){
                            childrenHtml += '<b>' + data[index].relativeFullName + '</b>&nbsp;&nbsp;';
                        }
                    });
                }   
                                
               
                if(fatherHtml!=''){
                    fathersName = 'Your father already added in the family Tree: '+fatherHtml;
                    $("#fatherHtml").html(fathersName);
                    document.getElementById("father").disabled = true;
                }
                if(motherHtml!=''){
                    mothersName = 'Your mother already added in the family Tree: '+motherHtml;
                    $("#motherHtml").html(mothersName);
                    document.getElementById("mother").disabled = true;
                }
                if(spouseHtml!=''){
                    spouseName = 'Your spouse already added in the family Tree: '+ spouseHtml;
                    $("#spouseHtml").html(spouseName);
                    document.getElementById("spouse").disabled = true;    
                }
                if(childrenHtml!=''){
                    childrensName = 'Your children already added in the family Tree: '+childrenHtml;    
                    $("#childrenHtml").html(childrensName);
                }
            }
           });

       
            //changing the hidden field relation value according to button clicked
            $("#father").click(function (){
                
                $("#relationship").val("father");

                
                $("#existingRelationship").val("father");

                
                $("#deadRelationship").val("father");

                $('#btnDead').attr('href', '/add-members/father/dead');
            });
            $("#mother").click(function (){
                
                $("#relationship").val("mother");

                
                $("#existingRelationship").val("mother");

                
                $("#deadRelationship").val("mother");
                $('#btnDead').attr('href', '/add-members/mother/dead');
            });
            $("#spouse").click(function (){
                
                $("#relationship").val("spouse");

                
                $("#existingRelationship").val("spouse");

                
                $("#deadRelationship").val("spouse");
                $('#btnDead').attr('href', '/add-members/spouse/dead');
            });
            $("#children").click(function (){
                $("#relationship").val("children");

                $("#existingRelationship").val("children");

                $("#deadRelationship").val("children");
                $('#btnDead').attr('href', '/add-members/children/dead');
            });


            $(".member").click(function () {
            //alert(this.id);
               // $("#invite").attr('href','/add-members/'+this.id+'/invite');
               // $("#exist").attr('href','/add-members/'+this.id+'/exist');
                $("#dead").attr('href','/add-members/'+this.id+'/dead');
                $("#AddOption").modal('show');
            });

            $("#invite").click(function(){
                $("#AddOption").modal('hide');
                $("#inviteModal").modal('show');
            });
            $("#exist").click(function(){
                $("#AddOption").modal('hide');
                $("#existModal").modal('show');
            });
            
			relativeId = '';
			relativeName ='';
            relativeType = '';
            $.ajax({
                url: '/GetRelatives',
                type:'POST',
				
                success: function(data) {    
                   $.each(data, function(index) {
                        relativeId += data[index].id;
						relativeName += data[index].relativeFullName;
						relativeType += data[index].relativeType;
                    });
                   $("#add-member-content").show();
                   $('#spinner').hide();
                },
                failure: function (data) {

                }
        }); 
    });
</script>
@stop