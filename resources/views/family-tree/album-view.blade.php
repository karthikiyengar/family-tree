
@extends('family-tree/master')

@section('master-header')
<link href="/assets//css/demo.css" rel="stylesheet">
<link href="/assets//css/uploader.css" rel="stylesheet"> 
<link rel="stylesheet" href="/assets//css/glisse.css" />
<meta name="profileId" content="{{ $profileId }}" />
@show



@section('content')
<!-- Bug in master page -->

<h5 style="margin-top:20px">{{ Session::get('status') }}</h5>
<br/><br/>
<h3>All Pictures</h3>
<div id="currentEventsContainer">
   <div id="spinner" style="display:none;text-align:center">
      <h5>Loading, Please Wait.<h5>
          <img src="/assets/img/spinner.gif" style="height:5%;width:5%">
      </div>
      <div id="currentEvents" class="col-lg-8 col-md-offset-1">
      </div>    
  </div>
</div>
@stop
@section('scripts')
<script type="text/javascript">
require(['jquery', 'demo.preview', 'dmuploader', 'jquery.migrate', 'glisse'], function($) {

    loadPictures();

    function loadPictures() {
        $.ajaxSetup({
            headers: {
                'X-CSRF-Token': $('meta[name=_token]').attr('content')
            }
        });
        var strId = $('meta[name=profileId]').attr('content');
        $.ajax({
            url: '/album/loadPhotos',
            type: 'POST',
            data:{'id' : strId},
            beforeSend: function() {
                $("#spinner").show();
            },
            success: function(data) {
                jsonData = JSON.parse(data);
                imageHTML = "";
                $.each(jsonData, function(index) {
                    imageHTML += "<div class='thumbnail'><img src='/" + jsonData[index]['path'] + "' class='myphotos' rel='group1' data-glisse-big='/" + jsonData[index]['path'] + "'/></div>";
                });
                $("#currentEvents").html(imageHTML);
                $("#spinner").hide();
                $('.myphotos').glisse({
                    speed: 500,
                    changeSpeed: 550,
                    effect: 'roll',
                    fullscreen: false
                });
            },
            failure: function() {
                alert('Try again.');
            }

        });
    }
});
</script>
@stop