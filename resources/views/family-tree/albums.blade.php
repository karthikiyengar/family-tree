
@extends('family-tree/master')

@section('master-header')
  <link href="/assets/css/demo.css" rel="stylesheet" />
  <link href="/assets/css/uploader.css" rel="stylesheet" /> 
  <link rel="stylesheet" href="/assets/css/glisse.css" />
@stop



@section('content')
	<!-- Bug in master page -->
	
    <h5 style="margin-top:20px">{{ Session::get('status') }}</h5>
    <br/><br/>
    <div class="container demo-wrapper">
     <div class="row demo-columns">
        <div class="col-md-6">
          <!-- D&D Zone-->
          <div id="drag-and-drop-zone" class="uploader">
            <div>Drag &amp; Drop Images Here</div>
            <div class="or">-or-</div>
            <div class="browser">
              <label>
                <span>Click to open the file Browser</span>
                <input type="file" name="files[]" multiple="multiple" title='Click to add Files'>
              </label>
            </div>
          </div>
          <!-- /D&D Zone -->

          <!-- Debug box -->
          <!-- <div class="panel panel-default">
            <div class="panel-heading">
              <h3 class="panel-title">Debug</h3>
            </div>
            <div class="panel-body demo-panel-debug">
              <ul id="demo-debug">
              </ul>
            </div>
          </div> -->
          <!-- /Debug box -->
        </div>
        <!-- / Left column -->

        <div class="col-md-5 ">
          <div class="panel panel-default ">
            <div class="panel-heading">
              <h3 class="panel-title">Uploads</h3>
            </div>
            <div class="panel-body demo-panel-files" id='demo-files'>
              <span class="demo-note">No Files have been selected/droped yet...</span>
            </div>
          </div>
        </div>
        <!-- / Right column -->
      </div>
    </div>
    <h3>All Pictures</h3>
    <div id="currentEventsContainer">
    	<div id="spinner" style="display:none;text-align:center">
    		<h5>Loading, Please Wait.<h5>
    		<img src="/assets/img/spinner.gif" style="height:5%;width:5%">
    	</div>
	    <div id="currentEvents" class="col-lg-8 col-md-offset-1">
	    </div>    
    </div>
</div>
@stop
@section('scripts')
<script src="/assets/js/load-images.js"></script>
@stop