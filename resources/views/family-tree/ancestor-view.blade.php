@extends('family-tree/master')
@section('master-header')
	<link rel="stylesheet" type="text/css" href="/assets/css/cytoscape.css">
    <meta name="ancestorId" content="{{ $ancestorId }}" />
@stop
@section('content')
    <h3>My Tree</h3>
    <div id="spinner" style="display:none;text-align:center">
    		<h5>Loading, Please Wait.<h5>
    		<img src="/assets/img/spinner.gif" style="height:5%;width:5%">
    </div>
    <div id="buttons" style="display:none;text-align:center">
    	<button id="btnViewProfile" class="btn btn-primary">View Profile</button>
    	<button id="btnViewTree" class="btn btn-primary">View Tree</button>
    	<button id="btnHome" class="btn btn-success" style="display:none">Home</button>
    </div>

    <div id="cy"></div>
@stop
@section('scripts')
<script src="/assets/js/ancestor-view.js"></script>
@stop
