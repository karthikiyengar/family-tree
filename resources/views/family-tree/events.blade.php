@extends('family-tree/master')
@section('content')
	
	
    <button style="margin-top:30px" id="btnAddEvent" class="btn text-center btn-primary">Add Events</button>
    <h5 style="margin-top:10px">{{ Session::get('status') }}</h5>
    <div id = "addEventDiv" style="display:none">
	    <h3>Add Events</h3>
	    <div class="row mt">
	        <div class="col-lg-8 col-md-offset-1">
	            <div class="form-panel">

	                <form class="form-horizontal style-form" method="post" action="addnewevent" data-parsley-validate>
	                    <input type="hidden" name="_token" value="{{{ csrf_token() }}}"/>
	                    <div class="form-group">
	                        <label class="col-sm-3 control-label">Event Name</label>

	                        <div class="col-sm-9">
	                            <input type="text" class="form-control" id="eventName" name="eventName" placeholder="Add your event name here" required>
	                        </div>
	                    </div>

	                    <div class="form-group">
	                        <label class="col-sm-3 control-label">Event Description</label>

	                        <div class="col-sm-9">
	                            <textarea rows="4" class="form-control" id="eventDescription" name="eventDescription" placeholder="Event Description" required></textarea>
	                        </div>
	                    </div>

	                    <div class="form-group">
	                        <label class="col-sm-3 control-label">Venue</label>

	                        <div class="col-sm-9">
	                            <textarea rows="4" class="form-control" id="eventVenue" name="eventVenue" placeholder="Event Venue" required></textarea>
	                        </div>
	                    </div>

	                    <div class="form-group">
	                        <label class="col-sm-3 control-label">Select Date</label>

	                        <div class="col-sm-9">
	                            <input type="date" class="form-control" id="eventDate" name="eventDate" required>
	                        </div>
	                    </div>

	                    <div class="form-group">
	                        <label class="col-sm-3 control-label"></label>

	                        <div class="col-sm-9">
	                            <button type="submit" class="btn btn-theme">Submit</button>
	                        </div>
	                    </div>
	                </form>
	            </div>
	        </div>
	    </div>
    </div>
    <h3>Your Events</h3>
    <div id="currentEventsContainer">
    	<div id="spinner" style="display:none;text-align:center">
    		<h5>Loading, Please Wait.<h5>
    		<img src="/assets/img/spinner.gif" style="height:5%;width:5%">
    	</div>
	    <div id="currentEvents" class="col-lg-8 col-md-offset-1">
	    </div>    
    </div>
@stop
@section('scripts')
    <script src="/assets/js/events.js"></script>
@stop
