@extends('family-tree.forms.form-master')
@section('form-div')
<div>
@stop
@section('form-header')
    Add {{$member}}
@stop
@section('personal-details')
<div class="form-group">
      <label class="col-sm-4 col-sm-4 control-label">
        Date of Death*
      </label>
      <div class="col-sm-8">
        <input type="date" class="form-control" placeholder="Date of Death" id="dateOfDeath" name="dateOfDeath" required>
      </div>
</div>
@stop
@section('form-main-hidden-fields')
	<input type="hidden", name="relationship" value="{{$member}}">
	<input type="hidden", name="type" value="dead">
@stop