<div id="spinner" style="display:none;text-align:center">
    <h5>Loading, Please Wait.<h5>
    <img src="/assets/img/spinner.gif" style="height:5%;width:5%">
</div>
<div class="row mt">
  <div class="col-lg-8 col-md-offset-1">
    <div class="form-panel">
      <form method="post" id="profile-form" enctype="multipart/form-data" data-parsley-validate>
        <div class="panel panel-default">
          <div class="panel-heading">
            Personal
          </div>
          <div class="panel-body">
            <div class="form-group">
              <label class="col-sm-4 col-sm-4 control-label">
                First Name*
              </label>
              
              <div class="col-sm-8">
                <input type="text" class="form-control" placeholder="First Name" id="firstName" name="firstName" maxlength="40" required>
              </div>
            </div>
            
            <div class="form-group">
              <label class="col-sm-4 col-sm-4 control-label">
                Last Name*
              </label>
              
              <div class="col-sm-8">
                <input type="text" class="form-control" placeholder="Last Name" id="lastName" name="lastName" maxlength="40" required>
              </div>
            </div>
            

            <div class="form-group">
              <label class="col-sm-4 col-sm-4 control-label">
                About Me*
              </label>
              
              <div class="col-sm-8">
                <textarea class="form-control" rows="5" name="aboutMe" id="aboutMe" required maxlength="150"></textarea> 
              </div>
            </div>

            <div class="form-group">
              <label class="col-sm-4 col-sm-4 control-label">
                Gender*
              </label>
              
              <div class="col-sm-8">
                <select class="form-control" id="gender" name="gender" required>
                  <option value="">
                    Select gender
                  </option>
                  <option value="Male">
                    Male
                  </option>
                  <option value="Female">
                    Female
                  </option>
                </select>
              </div>
            </div>
            
            <div class="form-group">
              <label class="col-sm-4 col-sm-4 control-label">
                Marital Status*
              </label>
              
              <div class="col-sm-8">
                <select class="form-control" id="maritialStatus" name="maritialStatus" required>
                  <option value="">
                    Select maritial status
                  </option>
                  <option value="Single">
                    Single
                  </option>
                  <option value="Married">
                    Married
                  </option>
                </select>
              </div>
            </div>
            
            
            <div class="form-group">
              <label class="col-sm-4 col-sm-4 control-label">
                Image
              </label>
              
              <div class="col-sm-8">
                <input type="file" class="form-control" id="profileImage" name="profileImage" accept="image/*">
              </div>
            </div>
            
            <div class="form-group">
              <label class="col-sm-4 col-sm-4 control-label">
                Date of Birth*
              </label>
              <div class="col-sm-8">
                <input type="date" class="form-control" placeholder="Date of birth" id="dateOfBirth" name="dateOfBirth" required>
              </div>
            </div>
            @yield('personal-details')
            
            <div class="form-group">
              <label class="col-sm-4 col-sm-4 control-label">
                Village
              </label>
              
              <div class="col-sm-8">
                <input type="text" class="form-control" placeholder="Village" id="village" name="village" maxlength="40">
              </div>
            </div>
            
            <div class="form-group">
              <label class="col-sm-4 col-sm-4 control-label">
                Mobile
              </label>
              
              <div class="col-sm-8">
                <input type="text" maxlength="10" data-parsley-pattern="^\d{10}$" class="form-control" placeholder="Mobile" id="mobileNumber" name="mobileNumber" disabled>
              </div>
            </div>

            <div class="form-group">
              <label class="col-sm-4 col-sm-4 control-label">
                Email Id
              </label>
              
              <div class="col-sm-8">
                <input type="email" maxlength="100"  class="form-control" placeholder="Email" id="email" name="email">
              </div>
            </div>

            <div class="form-group">
              <label class="col-sm-4 col-sm-4 control-label">
                Current Area*
              </label>
              
              <div class="col-sm-8">
                <input type="text" class="form-control" placeholder="Current Area" id="currentArea" name="currentArea" maxlength="40" required>
              </div>
            </div>
            
            <div class="form-group">
              <label class="col-sm-4 col-sm-4 control-label">
                Current City*
              </label>
              
              <div class="col-sm-8">
                <input type="text" class="form-control" placeholder="Current City" id="currentCity" name="currentCity" maxlength="40" required>
              </div>
            </div>
            
            <div class="form-group">
              <label class="col-sm-4 col-sm-4 control-label">
                Blood Group
              </label>
              
              <div class="col-sm-8">
                <select class="form-control" id="bloodGroup" name="bloodGroup">
                  <option value="">
                    Select blood group
                  </option>
                  <option>
                    A-
                  </option>
                  <option>
                    A+
                  </option>
                  <option>
                    B+
                  </option>
                  <option>
                    B-
                  </option>
                  <option>
                    AB+
                  </option>
                  <option>
                    AB-
                  </option>
                  <option>
                    O+
                  </option>
                  <option>
                    O-
                  </option>
                </select>
              </div>
            </div>
          </div>
        </div>
        
        <div class="panel panel-default">
          <div class="panel-heading">
            Professional
          </div>
          <div class="panel-body">
            <div class="form-group">
              <label class="col-sm-4 col-sm-4 control-label">
                Educational Level
              </label>
              
              <div class="col-sm-8">
                <select class="form-control" id="educationLevel" name="educationLevel">
                  <option value="">
                    Select education level
                  </option>
                  <option>
                    Graduate
                  </option>
                  <option>
                    Post-Graduate
                  </option>
                  <option>
                    High-School
                  </option>
                  <option>
                    School
                  </option>
                </select>
              </div>
            </div>
            <div class="form-group">
              <label class="col-sm-4 col-sm-4 control-label">
                Education Specialization
              </label>
              <div class="col-sm-8">
                <input type="text" class="form-control" placeholder="Education Specialization" id="educationSpecialization" name="educationSpecialization">
              </div>
            </div>
            <div class="form-group">
              <label class="col-sm-4 col-sm-4 control-label">
                Occupation
              </label>
              
              <div class="col-sm-8">
                  <input type="text" class="form-control" placeholder="Occupation" id="occupation" name="occupation" maxlength="40">
              </div>
            </div>
          </div>
        </div>
        <div class="panel panel-default">
          <div class="panel-heading">
            Family Details
          </div>
          <div class="panel-body">
            <div class="form-group">
              <label class="col-sm-4 col-sm-4 control-label">
                Mother's Name*
              </label>
              
              <div class="col-sm-8">
                <input type="text" class="form-control" placeholder="Mother's Name" id="mothersName" name="mothersName" maxlength="40" required>
              </div>
            </div>
            <div class="form-group">
              <label class="col-sm-4 col-sm-4 control-label">
                Mother's Father's Name
              </label>
              
              <div class="col-sm-8">
                <input type="text" class="form-control" placeholder="Mother's Father's Name" id="mothersFathersName" name="mothersFathersName" maxlength="40">
              </div>
            </div>
            <div class="form-group">
              <label class="col-sm-4 col-sm-4 control-label">
                Mother's Mother's Name
              </label>
              
              <div class="col-sm-8">
                <input type="text" class="form-control" placeholder="Mother's Mother's Name" id="mothersMothersName" name="mothersMothersName" maxlength="40">
              </div>
            </div>
            <div class="form-group">
              <label class="col-sm-4 col-sm-4 control-label">
                Mother's Father's Village
              </label>
              
              <div class="col-sm-8">
                <input type="text" class="form-control" placeholder="Mother's Father's Village" id="mothersFathersVillage" name="mothersFathersVillage" maxlength="40">
              </div>
            </div>
            <div class="form-group">
              <label class="col-sm-4 col-sm-4 control-label">
                Mother's Mother's Village
              </label>
              
              <div class="col-sm-8">
                <input type="text" class="form-control" placeholder="Mother's Mother's Village" id="mothersMothersVillage" name="mothersMothersVillage" maxlength="40">
              </div>
            </div>
            <div class="form-group">
              <label class="col-sm-4 col-sm-4 control-label">
                Father's Name*
              </label>
              
              <div class="col-sm-8">
                <input type="text" class="form-control" placeholder="Father's Name" id="fathersName" name="fathersName" maxlength="40" required>
              </div>
            </div>
            
            
            <div class="form-group">
              <label class="col-sm-4 control-label">
                Father's Father's Name
              </label>
              
              <div class="col-sm-8">
                <input type="text" class="form-control" placeholder="Father's Father's Name" id="fathersFathersName" name="fathersFathersName" maxlength="40">
              </div>
            </div>
            
            
            <div class="form-group">
              <label class="col-sm-4 col-sm-4 control-label">
                Father's Mother's Name
              </label>
              
              <div class="col-sm-8">
                <input type="text" class="form-control" placeholder="Father's Mother's Name" id="fathersMothersName" name="fathersMothersName" maxlength="40">
              </div>
            </div>
            
            <div id="buttonPanel" class="form-group">
              <div class="col-sm-8">
                <input type="submit" class="btn btn-theme" value="Save">
              </div>
            </div>
          </div>
        </div>
        <input type="hidden" name="_token" value="{{csrf_token()}}">
        @yield('form-main-hidden-fields')
      </form>
    </div>
  </div>
  <!-- col-lg-12-->
</div>
