@extends('family-tree.master')
@section('content')
	<h2>@yield('form-header')</h2>
	<div id="spinner" style="display:none;text-align:center">
    	<h5>Loading, Please Wait.<h5>
	    <img src="/assets/img/spinner.gif" style="height:5%;width:5%">
	</div>
	@yield('form-div')
    @include('family-tree.forms.form-main')
    </div>
@stop
	