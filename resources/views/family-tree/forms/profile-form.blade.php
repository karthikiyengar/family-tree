<!-- Used for the profile page which needs to be filled by each user.
     Also contains script to pull existing data for edit -->

@extends('family-tree.forms.form-master')
@section('form-header')
    Profile Form
    <h5>{{$status}}</h5>
@stop
@section('form-div')
<div id="main-form" style="display:none">
@stop
@section('scripts')
<script src="/assets/js/form.js"></script>
@stop
