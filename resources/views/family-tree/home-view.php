<script id="entry-template" type="text/x-handlebars-template">
<br/><br/>
<button id="btnSendSms" class="btn btn-primary"  data-toggle="modal" data-target="#sendSmsModal">Send SMS</button>

<div id="sendSmsModal" class="modal fade">
	<div class="modal-dialog">
		<div class="modal-content">
			<div class="modal-header">
				<button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
				<h4 class="modal-title">Confirmation</h4>
			</div>
			<div class="modal-body">
				<p>Select Recepients</p>
				<form class="form-horizontal">
					<div id="reciever" class="well" style="max-height: 200px; overflow: auto;">				
						
					</div>
					<textArea id="txtMessage" class="form-control">This is a test message</textArea>
				</form>

			</div>
			<div class="modal-footer">
				<button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
				<button type="button" id="btnModalSend" class="btn btn-primary" data-dismiss="modal">Send</button>
			</div>
		</div>
	</div>
</div>
{{#each .}}
<div class="col-md-6 col-md-offset-1">
	<div class="row bdbg">
		<div class="col-md-5">
			<div class="row">
				<div class="col-md-5"><a href="profile.html"><img src="profilepics/{{creatorProfileImage}}" class="img-circle" width="60"></a></div>
				<div class='col-md-9'><p class='para'>{{creatorName}}</p></div>
			</div>
		</div>
		<div class="col-md-7">
			<div class="row">
				<div class="col-md-12">
					<h4 class="heading">{{eventName}}</h4>
				</div>
				<div class="col-md-12">
					<p class="dtl">Venue:&nbsp;{{eventVenue}}</p>
				</div>
				<div class="col-md-12">
					<p class="dtl">Description:&nbsp;{{eventDescription}}</p>
				</div>
				<div class="col-md-12">
					<p class="dtl">Date:&nbsp;{{eventDate}}</p>
				</div>
			</div>	
		</div>
		
		<div class="row">
			<div class="col-md-12">
				<div class="show-comments" name="{{eventId}}"><span class="glyphicon glyphicon-comment"> Toggle Comments</div>
			</div>
		</div>
		<div class="comment-section comment-section-{{eventId}}" style='display:none'>
		{{#comments}}
		<div class="row">
			<div class="col-md-1"><img src="/profilepics/{{profileImage}}" class="img-circle" width="30">
			</div>
			<div class="col-md-11">
				<div class="row">
					<div class="col-md-12"><span class="para">{{commentedByName}} - </span><span class="dtl">{{commentMessage}}</span>
					</div>
					<div class="col-md-12"><span class="para">Comment on :  {{timestamp}}</span> 
                    </div>
				</div>
				<hr>
			</div>
		</div>
		{{/comments}}
		</div>
		<div class='row'>
                <div class='col-md-1'><img class='img-circle commentImg' width='30'>
                </div>
                <form method='POST' class="form-comment" action='/addComment'>
                <div class='col-md-8'><input type='text' class='form-control' name='commentMessage' placeholder='Comment' required maxlength='200'>
                </div>
               <div class='col-md-3'><button type='submit' class='btn btn-theme btn-submit' style='margin:0px;'>Comment</button>
            	<input type='hidden' name='eventId' value="{{eventId}}">
               <input type='hidden' class="commentedBy" name='commentedBy'>
               <input type='hidden' class="csrfToken" name='_token'>
                </div></form>
              </div>
          </div>
	</div>
{{/each}}
</script>