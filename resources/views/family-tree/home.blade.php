@extends('family-tree/master')
@section('master-header')
<link href="assets/css/mystyle.css" rel="stylesheet">
@stop
@section('content')

<div id="spinner" style="display:none;text-align:center">
	<h5>Loading, Please Wait.<h5>
	<img src="/assets/img/spinner.gif" style="height:5%;width:5%">
</div>
@stop
@section('scripts')
@include('family-tree/home-view')
<script src="/assets/js/home.js"></script>
@stop