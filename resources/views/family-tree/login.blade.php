<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta name="description" content="">
    <meta name="author" content="Dashboard">
    <meta name="keyword" content="Dashboard, Bootstrap, Admin, Template, Theme, Responsive, Fluid, Retina">
    <meta name="_token" content="{{ csrf_token() }}"/>

    <title>DASHGUM - Bootstrap Admin Template</title>

    <!-- Bootstrap core CSS -->
    <link href="assets/css/bootstrap.css" rel="stylesheet">
    <!--external css-->
    <link href="assets/font-awesome/css/font-awesome.css" rel="stylesheet"/>

    <!-- Custom styles for this template -->
    <link href="assets/css/style.css" rel="stylesheet">
    <link href="assets/css/style-responsive.css" rel="stylesheet">

    <!-- HTML5 shim and Respond.js IE8 support of HTML5 elements and media queries -->
    <!--[if lt IE 9]>
    <!-- <script src="https://oss.maxcdn.com/libs/html5shiv/3.7.0/html5shiv.js"></script>
    <script src="https://oss.maxcdn.com/libs/respond.js/1.4.2/respond.min.js"></script> -->
    <![endif]-->
</head>
<body>

<!-- **********************************************************************************************************************************************************
MAIN CONTENT
*********************************************************************************************************************************************************** -->

<div id="login-page">
    <div class="container">

        <form class="form-login" method="post" action="/login" id="form-login" data-parsley-validate>
            <h2 class="form-login-heading">sign in now</h2>

            <div class="login-wrap">
                <input id="txtPhone" name="txtPhone" type="text" class="form-control" placeholder="Mobile No."
                       autofocus maxlength="10" data-parsley-pattern="^\d{10}$" data-parsley-group="block1" required >
                <label class="checkbox">
                        {{ Session::get('status')}}
                        <span class="pull-right">
                            <input type="button" class="btn btn-theme btn-block" id="getPin" value="Get Pin"/>        
                        </span>
                </label>
                <br>
                <input id="txtPin" name="txtPin" type="password" class="form-control" placeholder="Pin" maxlength="5" data-parsley-pattern="^\d{5}$" data-parsley-group="block2" required >
                
                <input class="btn btn-theme btn-block" type="submit" id="btnLogin" name="btnLogin">
            </div>

            <!-- Modal -->
            <!-- <div aria-hidden="true" aria-labelledby="myModalLabel" role="dialog" tabindex="-1" id="myModal"
                 class="modal fade">
                <div class="modal-dialog">
                    <div class="modal-content">
                        <div class="modal-header">
                            <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
                            <h4 class="modal-title">Forgot Password ?</h4>
                        </div>
                        <div class="modal-body">
                            <p>Enter your e-mail address below to reset your password.</p>
                            <input type="text" name="email" placeholder="Email" autocomplete="off"
                                   class="form-control placeholder-no-fix">

                        </div>
                        <div class="modal-footer">
                            <button data-dismiss="modal" class="btn btn-default" type="button">Cancel</button>
                            <button class="btn btn-theme" type="button">Submit</button>
                        </div>
                    </div>
                </div>
            </div> -->
            <!-- modal -->
            <input type="hidden" name="_token" value="{{{ csrf_token() }}}"/>
        </form>

    </div>
</div>

<!-- js placed at the end of the document so the pages load faster -->
<script src="assets/js/lib/require.js"></script>
<script src="assets/js/app.js"></script>
<!--BACKSTRETCH-->
<!-- You can use an image of whatever size. This script will stretch to fit in any screen size.-->
<!-- <script type="text/javascript" src="assets/js/jquery.backstretch.min.js"></script> -->
<script src="assets/js/login.js"></script>
</body>
</html>