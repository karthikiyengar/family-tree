<!DOCTYPE html>
<html lang="en">

<head>
    <meta charset="utf-8">
    <!--<meta name="viewport" content="width=device-width, initial-scale=1.0">-->
    <meta name="_token" content="{{ csrf_token() }}" />
    <meta name="id" content="{{ Session::get('id') }}" />
    <meta name="fullName" content="{{ Session::get('fullName') }}" />
    <meta name="parent_id" content="{{ Session::get('parent_id') }}" />
    <meta name="parentFullName" content="{{ Session::get('parentFullName') }}" />
    <meta name="parentProfileImage" content="{{ Session::get('parentProfileImage') }}" />
    <meta name="profileImage" content="{{ Session::get('profileImage') }}" />

    <title>Family Tree - Index</title>
    <link href="/assets/css/bootstrap.css" rel="stylesheet">
    <link href="/assets//font-awesome/css/font-awesome.css" rel="stylesheet" />
    <link rel="stylesheet" type="text/css" href="/assets//css/zabuto_calendar.css">
    <link href="/assets//css/style.css" rel="stylesheet">
    <link href="/assets//css/autocomplete.css" rel="stylesheet">
    <link href="/assets//css/style-responsive.css" rel="stylesheet">
    

    @yield('master-header')
</head>

<body>


    <!-- **********************************************************************************************************************************************************
TOP BAR CONTENT & NOTIFICATIONS
*********************************************************************************************************************************************************** -->
    <!--header start-->
    @section('header')
    <div class="header black-bg">
        <div class="sidebar-toggle-box">
            <div class="fa fa-bars tooltips" data-placement="right" data-original-title="Toggle Navigation"></div>
        </div>
        <!--logo start-->
        <a href="/" class="logo"><img src="/assets/img/flogo.png"><b>Family Tree    </b></a>
        <!--logo end-->

        <div class="top-menu">
           <!--  <div id=""class="nav pull-right top-menu">
                <select id="profileList">
                </select>           
                
         
            </div>
            <ul class="nav pull-right top-menu">
                <li><a class="logout" href="/logout">Logout</a></li>
            </ul>
        </div> -->


            <div class="dropdown pull-right"  style="margin:15px;">
                <button class="btn btn-default dropdown-toggle" type="button" data-toggle="dropdown" aria-expanded="true">
                    <img src="/profilepics/{{Session::get('profileImage')}}" class="img-circle" width="30"> {{Session::get('fullName')}}
                    <span class="caret"></span>
                </button>
                <ul class="dropdown-menu" id="profileList" role="menu" aria-labelledby=""> 

                    </ul>                  
            </div>


        </div>
       
        <div class="nav notify-row" id="top_menu">
            <ul class="nav top-menu">
                <li>
                   <div class="navbar-form navbar-left" role="search">
                        <div class="input-group">
                            <div id="searchfield">
                                <input id="txtSearch" type="text" class="form-control" placeholder="Search" autocomplete="off">
                            </div>
                            <div id="outputbox">
                                <p id="outputcontent"></p>
                            </div>
                        </div>
                    </div>
                </li>
            </ul>
        </div>
    </div>
    @show
    <!--header end-->

    <!-- **********************************************************************************************************************************************************
    MAIN SIDEBAR MENU
    *********************************************************************************************************************************************************** -->
    <!--sidebar start-->
    @section('sidebar')
    <div id="sidebar" class="nav-collapse ">
        <!-- sidebar menu start-->
        <ul class="sidebar-menu" id="nav-accordion">

            <p class="centered">
                <a href="/profile/{{Session::get('id')}}"><img src="/profilepics/{{Session::get('profileImage')}}" class="img-circle" width="60"></a>
            </p>
            <h5 class="centered">{{Session::get('fullName')}}</h5>

            @if(Session::get('adminApproval'))
            <li class="mt">
                <a class="{{ Request::is('home') ? 'active' : '' }}" href="/home">
                    <i class="fa fa-desktop"></i>
                    <span>Home</span>
                </a>
            </li>
            @endif

            <li class="sub-menu">
                <a class="{{ Request::is('profile') ? 'active' : '' }}" href="/profile">
                    <i class="fa fa-book"></i>
                    <span>Profile</span>
                </a>
            </li>
            @if(Session::get('adminApproval'))
            <li  class="sub-menu">
                <a class="{{ Request::is('my-tree') ? 'active' : '' }}" href="/my-tree">
                    <i class="fa fa-cubes"></i>
                    <span>My Tree</span>
                </a>
            </li>
               <li class="sub-menu">
                <a class="{{ Request::is('ancestor-view') ? 'active' : '' }}" href="/ancestor-view">
                    <i class="fa fa-cubes"></i>
                    <span>Ancestor View</span>
                </a>
            </li>

            <li class="sub-menu">
                <a class="{{ Request::is('add-members') ? 'active' : '' }}" href="/add-members">
                    <i class="fa fa-tasks"></i>
                    <span>Add Members</span>
                </a>
            </li>

            <li class="sub-menu">
                <a class="{{ Request::is('events') ? 'active' : '' }}" href="/events">
                    <i class="fa fa-calendar"></i>
                    <span>Events</span>
                </a>
            </li>

            <li class="sub-menu">
                <a class="{{ Request::is('albums') ? 'active' : '' }}" href="/albums">
                    <i class="fa fa-picture-o"></i>
                    <span>Albums</span>
                </a>
            </li>
            @endif
        </ul>
        <!-- sidebar menu end-->
    </div>
    @show
    <!--sidebar end-->

    <!-- **********************************************************************************************************************************************************
    MAIN CONTENT
    *********************************************************************************************************************************************************** -->
    <!--main content start-->
    <div class="wrapper">
        <div id="main-content">
            @yield('content')
        </div>
    </div>
    <!--main content end-->


    <!--footer start-->
    @section('footer')
    
    @show
    <!--footer end-->

    <!-- js placed at the end of the document so the pages load faster -->
    <script src="/assets/js/lib/require.js"></script>
    <script src="/assets/js/app.js"></script>
    <script src="/assets/js/master.js"></script>
    @yield('scripts')
</body>

</html>