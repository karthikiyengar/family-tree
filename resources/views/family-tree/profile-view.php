<div id="template-content">
</div>

<script id="entry-template" type="text/x-handlebars-template">
<hr class="">
<div class="wrapper">
  <div class="row">
    <div class="col-md-4 text-center">
      <img title="profile image" class="img-circle img-responsive center-block" style="height:150px;width:150px" src="/profilepics/{{profileImage}}">
      <h2>
        {{fullName}}
      </h2>

      <a href="/album/userAlbum?id={{id}}" class="btn btn-success">View Album</a>
      <br/>
      <br/>
      <b>
        {{relationString}}
      </b>
    </div>
    <div class="col-md-8">
      <a href="/users" class="pull-right">
      </a>
      <div class="panel panel-default">
        <div class="panel-heading">
          {{fullName}}&#39s Bio
        </div>
        <div class="panel-body">
          {{aboutMe}}
        </div>
      </div>
      <div class="panel panel-default">
        <div class="panel-heading">
          Profile
        </div>
        <ul class="list-group">
          <li class="list-group-item text-right">
            <span class="pull-left">
              <strong class="">
                Date of Birth
              </strong>
            </span>
            {{dateOfBirth}}&nbsp;
          </li>
          <li class="list-group-item text-right">
            <span class="pull-left">
              <strong class="">
                Gender
              </strong>
            </span>
            <span>{{gender}}&nbsp;</span>
          </li>
          <li class="list-group-item text-right">
            <span class="pull-left">
              <strong class="">
                Maritial Status
              </strong>
            </span>
            {{maritialStatus}}&nbsp;
          </li>
          <li class="list-group-item text-right">
            <span class="pull-left">
              <strong class="">
                Current City
              </strong>
            </span>
            {{currentCity}}&nbsp;
          </li>
          <li class="list-group-item text-right">
            <span class="pull-left">
              <strong class="">
                Occupation: 
              </strong>
            </span>
            {{occupation}}&nbsp;
          </li>
        </ul>
      </div>
    </div>
    <div class="col-md-12">
      <div class="panel panel-default">
        <div class="panel-heading">
          Family Details
        </div>
        <li class="list-group-item text-right">
          <span class="pull-left">
            <strong class="">
              Village
            </strong>
          </span>
          {{village}}&nbsp;
        </li>
        <li class="list-group-item text-right">
          <span class="pull-left">
            <strong class="">
              Mobile Number
            </strong>
          </span>
          {{mobileNumber}}&nbsp;
        </li>
        <li class="list-group-item text-right">
          <span class="pull-left">
            <strong class="">
              Email ID
            </strong>
          </span>
          {{email}}&nbsp;
        </li>
        <li class="list-group-item text-right">
          <span class="pull-left">
            <strong class="">
              Current Area
            </strong>
          </span>
          {{currentArea}}&nbsp;
        </li>
        <li class="list-group-item text-right">
          <span class="pull-left">
            <strong class="">
              Blood  Group
            </strong>
          </span>
          {{bloodGroup}}&nbsp;
        </li>
        <li class="list-group-item text-right">
          <span class="pull-left">
            <strong class="">
              Educational Level
            </strong>
          </span>
          {{educationLevel}}&nbsp;
        </li>
        <li class="list-group-item text-right">
          <span class="pull-left">
            <strong class="">
              Educational Specialization
            </strong>
          </span>
          {{educationSpecialization}}&nbsp;
        </li>
        <li class="list-group-item text-right">
          <span class="pull-left">
            <strong class="">
              Mother&#39;s Name
            </strong>
          </span>
          {{mothersName}}&nbsp;
        </li>
        <li class="list-group-item text-right">
          <span class="pull-left">
            <strong class="">
              Mother&#39;s Father&#39;s Name
            </strong>
          </span>
          {{fathersName}}&nbsp;
        </li>
        <li class="list-group-item text-right">
          <span class="pull-left">
            <strong class="">
              Mother&#39;s Mother&#39;s Name
            </strong>
          </span>
          {{mothersMothersName}}&nbsp;
        </li>
        <li class="list-group-item text-right">
          <span class="pull-left">
            <strong class="">
              Mother&#39;s Father&#39;s Village
            </strong>
          </span>
          {{mothersFathersName}}&nbsp;
        </li>
        <li class="list-group-item text-right">
          <span class="pull-left">
            <strong class="">
              Mother&#39;s Mother&#39;s Village
            </strong>
          </span>
          {{mothersMothersVillage}}&nbsp;
        </li>
        <li class="list-group-item text-right">
          <span class="pull-left">
            <strong class="">
              Father&#39;s Father&#39;s Name
            </strong>
          </span>
          {{fathersFathersName}}&nbsp;
        </li>
        <li class="list-group-item text-right">
          <span class="pull-left">
            <strong class="">
              Father&#39;s Mother&#39;s Name
            </strong>
          </span>
          {{fathersMothersName}}&nbsp;
        </li>
      </div>
    </div>
  </div>
</div>
</script>