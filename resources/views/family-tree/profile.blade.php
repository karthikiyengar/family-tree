@extends('family-tree/master')
@section('master-header')
<meta name="profileId" content="{{ $profileId }}" />
@stop
@section('content')
@include('family-tree/profile-view')
@stop
@section('scripts')
<script src="/assets/js/profile.js">
</script>
@stop